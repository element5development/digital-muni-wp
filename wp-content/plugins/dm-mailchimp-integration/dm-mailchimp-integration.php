<?php
/*
Plugin Name: Digital Muni Mailchimp Integration
Description: Send campaign e-mails via Mailchimp
Plugin URI: URI goes here
Author: Digamber Pradhan
Author URI: https://www.digamberpradhan/
Version: 1.0.0
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
Text Domain: plugin-text-domain
*/

/**
 * Define Plugin FILE PATH
 */
if ( ! defined( 'DM_MAILCHIMP_FILE_PATH' ) ) {
	define( 'DM_MAILCHIMP_FILE_PATH', __FILE__ );
}
if ( ! defined( 'DM_MAILCHIMP_DIR_PATH' ) ) {
	define( 'DM_MAILCHIMP_DIR_PATH', dirname( __FILE__ ) );
}
require_once( DM_MAILCHIMP_DIR_PATH . '/class/class-admin.php' );
require_once( DM_MAILCHIMP_DIR_PATH . '/class/class-dm-mailchimp.php' );


//add_action( 'init', function () {
//	$api_key  = 'e8efacb02f4a466b5e6a719e1556dbbb-us9';
//	$url      = 'https://us9.api.mailchimp.com/3.0/lists/7bba8ffa28';
//	$args     = $args = array(
//		'headers' => array(
//			'Authorization' => 'Basic ' . base64_encode( 'digamber89' . ':' . $api_key )
//		)
//	);
//	$response = wp_remote_get( $url, $args );
//	$body     = wp_remote_retrieve_body( $response );
//	var_dump( json_decode( $body ) );
//	die;
//} );
