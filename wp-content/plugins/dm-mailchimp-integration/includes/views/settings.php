<form id="" class="" action="" method="post">
	<?php wp_nonce_field( 'verify_plugin_settings_nonce', 'dm_mailchimp_settings_nonce' ); ?>
    <table class="form-table">
        <tr>
            <th><label for="mailchimp-url">Mailchimp URL</label></th>
            <td>
                <input type="text" id="mailchimp-url" name="mailchimp-url" required value="<?php echo $this->settings['mailchimp-url']; ?>"/>
            </td>
        </tr>
        <tr>
            <th><label for="from-name">From Name</label></th>
            <td>
                <input type="text" id="from-name" name="from-name" required value="<?php echo $this->settings['from-name']; ?>"/>
            </td>
        </tr>
        <tr>
            <th><label for="reply-to-email">Email</label></th>
            <td>
				<?php
				$email = '';
				if ( isset( $this->settings['reply-to-email'] ) && ! empty( $this->settings['reply-to-email'] ) ) {
					$email = $this->settings['reply-to-email'];
				}
				?>
                <input type="email" id="reply-to-email" name="reply-to-email" value="<?php echo $email; ?>" required/><br/>
                <span class="description">Reply to E-mail</span>
            </td>
        </tr>
        <tr>
			<?php
			$api_key = '';
			if ( isset( $this->settings['api-key'] ) && ! empty( $this->settings['api-key'] ) ) {
				$api_key = $this->settings['api-key'];
			}
			?>
            <th><label for="api-key">API Key</label></th>
            <td><input type="text" id="api-key" name="api-key" value="<?php echo $api_key; ?>" required/></td>
        </tr>
		<?php
		if ( ! empty( $this->settings['api-key'] ) ):
			?>
            <tr>
                <th><label for="list-id">List ID</label></th>
                <td>
                    <select id="list-id" name="list-id" class="select" required>
                        <option value="">Select A list</option>
						<?php
						$mailchimp = dm_mailchimp_api::get_instance();
						$lists     = json_decode( $mailchimp->get_lists() );
						foreach ( $lists->lists as $list ) {
							echo '<option value="' . $list->id . '" ' . selected( $list->id, $this->settings['list-id'] ) . '>' . $list->name . '</option>';
						}
						?>
                    </select>
                    <span class="description">Select the list where the notification emails should go to</span>
                </td>

            </tr>
            <tr>
                <th><label for="template-id">Template ID</label></th>
                <td>
					<?php
					$templates = json_decode( $mailchimp->get_templates() );
					?>
                    <select id="template-id" name="template-id" class="select" required>
                        <option value="">Select A Template</option>
						<?php
						$mailchimp = dm_mailchimp_api::get_instance();
						$templates = json_decode( $mailchimp->get_templates() );
						foreach ( $templates->templates as $template ) {
							echo '<option value="' . $template->id . '" ' . selected( $template->id, $this->settings['template-id'] ) . '>' . $template->name . '</option>';
						}
						?>
                    </select>
                    <span class="description">Please be sure to select the correct template</span>
                </td>

            </tr>
		<?php
		endif;
		?>
    </table>
    <p class="submit">
        <input type="submit" class="button button-primary" value="save"/>
    </p>
</form>