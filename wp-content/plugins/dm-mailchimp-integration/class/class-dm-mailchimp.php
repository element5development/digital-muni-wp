<?php

class dm_mailchimp_api {
	public static $_instance;
	private $api_key;
	private $url;
	private $request_args;

	public static function get_instance() {
		if ( ! isset( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function __construct() {
		$this->mail_sent    = false;
		$this->settings     = get_option( 'dm_mailchimp_settings' );
		$this->api_key      = $this->settings['api-key'];
		$this->url          = $this->settings['mailchimp-url'];
		$this->request_args = [ 'headers' => [ 'Authorization' => 'Basic ' . base64_encode( 'any_string' . ':' . $this->api_key ) ] ];


		add_action( 'save_post_job_listing', array( $this, 'new_job_listing' ), 10, 3 );
		add_action( 'post_submitbox_misc_actions', array( $this, 'project_email_submission' ) );
		add_action( 'user_register', array( $this, 'add_to_list' ), 10, 1 );
		//add_action( 'init', array( $this, 'add_to_list' ), 10, 1 );
	}

	public function add_to_list( $user_id ) {
		$request_args = $this->request_args;
		$list_id      = $this->settings['list-id'];
		$url          = $this->url . 'lists/' . $list_id;
		$user         = get_user_by( 'id', $user_id );

		$body = [
			'members'         => [
				[ 'email_address' => $user->user_email, 'status' => 'subscribed', ]
			],
			'update_existing' => true
		];

		$request_args['body'] = json_encode( $body );
		$response             = wp_remote_post( $url, $request_args );
		$response_body        = wp_remote_retrieve_body( $response );
		file_put_contents( DM_MAILCHIMP_DIR_PATH . '/logs/registered.txt', var_export( $response_body, true ) );
	}

	public function project_email_submission( $post ) {
		$post_id = $post;

		if ( $post->post_type != 'job_listing' ) {
			return;
		}

		$value = get_post_meta( $post_id, 'dm_send_project_email', true );
		wp_nonce_field( 'verify_dm_project_nonce', 'dm_project_nonce' );
		?>
        <div class="misc-pub-section misc-pub-section-last">
            <label>
                <select name="dm_send_project_email">
                    <option value="no">Don't Send Project E-mail</option>
                    <option value="yes">Send Project E-mail</option>
                </select>
            </label>
        </div>
		<?php
	}


	public function new_job_listing( $post_id, $post, $update ) {
		$dm_send_project_email = filter_input( INPUT_POST, 'dm_send_project_email' );

		if ( $this->mail_sent ) {
			return;
		}

		if ( wp_is_post_revision( $post_id ) || wp_is_post_autosave( $post_id ) ) {
			return;
		}

		if ( ! $update ) {
			// if new object
			return;
		}

		if ( empty( $dm_send_project_email ) ) {
			return;
		}

		if ( $dm_send_project_email == 'no' ) {
			return;
		}

		//have to have this to avoid multiple emails from being sent.
		$this->mail_sent = true;

		if ( $post->post_type == 'job_listing' && $post->post_status == 'publish' && wp_doing_ajax() == false ) {
			//And update the meta so it wont run again
			update_post_meta( $post_id, 'check_if_run_once', 'true' );
			$subject = $post->post_title;
			$this->create_and_send_campaign( $subject, $post );
		}
	}

	public function create_and_send_campaign( $subject, $post ) {

		$campaign_id = $this->create_campaign( $subject );

		//build content
		$acf               = $_POST['acf'];
		$underwriters      = $acf['field_5d0a4a9bb7888'];
		$mas               = $acf['field_5d0a4b082ce17'];
		$type_of_doc_field = $acf['field_5d0930be89b67'];

		$type_of_doc_data = get_term_by( 'id', $type_of_doc_field, 'job_listing_type' );

		$issue_description = $acf['field_5d162f082db9c'];

		$type_of_doc = ! empty( $type_of_doc_data ) ? $type_of_doc_data->name : ' ';

		$underwriters_content = '';
		$mas_content          = '';

		if ( ! empty( $underwriters ) ) {
			$underwriters_content .= '<ul class="ma-uw">';
			$underwriters_content .= '<li><strong>Underwriter(s)</strong></li>';
			foreach ( $underwriters as $underwriter ) {
				$underwriter = get_term_by( 'id', $underwriter, 'underwriter' );
				$image       = get_field( 'underwriters_featured_image', $underwriter );
				$image_url   = '';
//				if ( ! empty( $image ) ) {
//					$image_url = '<img src=' . $image . ' style="width:50px">';
//				}
				if ( ! empty( $underwriter ) ) {
					$underwriters_content .= '<li><div>' . $underwriter->name . '</div></li>';
				}
			}
			$underwriters_content .= '<div style="clear:both"></div></ul>';
		}
		if ( ! empty( $mas ) ) {
			$mas_content .= '<ul class="ma-uw">';
			$mas_content .= '<li><strong>Municipal Advisor(s)</strong></li>';
			foreach ( $mas as $ma ) {
				$ma        = get_term_by( 'id', $ma, 'ma' );
				$image     = get_field( 'ma_featured_image', $ma );
				$image_url = '';
//				if ( ! empty( $image ) ) {
//					$image_url = '<img src=' . $image . ' style="width:50px">';
//				}
				if ( ! empty( $ma ) ) {
					$mas_content .= '<li><div>' . $ma->name . '</div></li>';
				}
			}
			$mas_content .= '<div style="clear:both"></div></ul>';
		}


		$underwriters_content = $underwriters_content . ' ' . $mas_content;


		if ( empty( $issue_description ) ) {
			$issue_description = ' ';
		}

		$permalink = get_permalink();
		$link      = add_query_arg( [
			'accepted'   => 'true',
			'show_pdf'   => 'true',
			'project_id' => $post->ID
		], $permalink );

		$issue_description = apply_filters( 'the_content', $issue_description ) . '<br /> 
<a href="' . $link . '" style="font-size:15px">PDF Download Link</a>';

		$content = [
			'type_of_doc'          => $type_of_doc,
			'issue_description'    => $issue_description,
			'underwriter_mas_logo' => $underwriters_content
		];


		$response = json_decode( $this->set_campaign_content( $campaign_id, $content ) );

		if ( ! empty( $response->html ) ) {
			$response = $this->send_campaign( $campaign_id );
			file_put_contents( DM_MAILCHIMP_DIR_PATH . '/logs/log.txt', var_export( $response, true ), FILE_APPEND );
		}

	}

	public function create_campaign( $subject ) {
		$campaign_url = $this->url . 'campaigns';
		//keep static for now.
		$list_id = $this->settings['list-id'];
		if ( empty( $list_id ) ) {
			return false;
		}
		$subject   = esc_html( $subject );
		$reply_to  = $this->settings['reply-to-email'];
		$from_name = $this->settings['from-name'];

		$body = [
			'recipients' => array( 'list_id' => $list_id ),
			'type'       => 'regular',
			'settings'   => [
				'title'        => $subject,
				'subject_line' => $subject,
				'reply_to'     => $reply_to,
				'from_name'    => $from_name
			]
		];

		$request_args = $this->request_args;

		$request_args['body'] = json_encode( $body );
		$response             = wp_remote_post( $campaign_url, $request_args );
		$response_body        = wp_remote_retrieve_body( $response );

		if ( ! empty( $response_body ) ) {
			$campaign_details = json_decode( $response_body );

			return $campaign_details->id;
		}

		return false;
	}

	public function set_campaign_content( $campaign_id, $content ) {

		if ( ! $campaign_id ) {
			return false;
		}

		$template_id = (int) $this->settings['template-id'];

		if ( empty( $template_id ) ) {
			return new WP_Error( 'TPID', 'Template ID Missing' );
		}

		$campaign_content_url   = $this->url . 'campaigns/' . $campaign_id . '/content';
		$request_args           = $this->request_args;
		$request_args['method'] = 'PUT';
		$template_content       = [
			'template' => [
				// The id of the template to use.
				'id'       => $template_id, // INTEGER
				// Content for the sections of the template. Each key should be the unique mc:edit area name from the template.
				'sections' => $content
			]
		];
		$request_args['body']   = json_encode( $template_content );
		$response               = wp_remote_post( $campaign_content_url, $request_args );

		return wp_remote_retrieve_body( $response );
	}

	public function send_campaign( $campaign_id ) {
		$response = wp_remote_post( $this->url . 'campaigns/' . $campaign_id . '/actions/send', $this->request_args );

		return $response;
	}

	public function get_lists() {
		$url                  = $this->url . '/lists/';
		$request_args         = $this->request_args;
		$url = add_query_arg( 'count', 50, $url );
		//$request_args['body'] = json_encode( [ 'count' => 100 ] );
		$response             = wp_remote_get( $url, $request_args );
		$response_body        = wp_remote_retrieve_body( $response );
		$response_var = json_decode( $response_body );


		return $response_body;
	}

	public function get_templates() {
		$url          = $this->url . '/templates/?type=user';
		$request_args = $this->request_args;
		add_query_arg( 'type', 'user', $url );


		$response = wp_remote_get( $url, $request_args );

		return wp_remote_retrieve_body( $response );
	}
}

add_action( 'plugins_loaded', array( 'dm_mailchimp_api', 'get_instance' ) );