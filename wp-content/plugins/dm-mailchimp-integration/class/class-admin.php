<?php

/**
 * Class plugin_admin
 */
class plugin_admin {
	public static $instance;
	public $settings = '';
	public $plugin_url = 'dm-mailchimp-settings';
	private $message = null;

	/**
	 * @return plugin_admin
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * plugin_admin constructor.
	 */
	public function __construct() {
		/*load dependencies if required*/
		$this->load_dependencies();
		add_action( 'admin_menu', array( $this, 'admin_menu_page' ) );
		add_action( 'admin_init', array( $this, 'save_settings' ) );

	}

	public function load_dependencies() {

	}

	public function admin_menu_page() {
		add_menu_page(
			'DM Mailchimp Integration',
			'DM Mailchimp Integration',
			'manage_options',
			$this->plugin_url,
			array( $this, 'generate_admin_page' )
		);

	}

	public function generate_admin_page() {
		require_once( DM_MAILCHIMP_DIR_PATH . '/includes/views/admin.php' );
	}

	public function save_settings() {
		if ( ! empty( $_POST['dm_mailchimp_settings_nonce'] ) && wp_verify_nonce( $_POST['dm_mailchimp_settings_nonce'], 'verify_plugin_settings_nonce' ) ) {
			/*Bail Early*/
			if ( ! current_user_can( 'manage_options' ) ) {
				return false;
			}
			$config = [];

			$config['mailchimp-url']  = filter_input( INPUT_POST, 'mailchimp-url' );
			$config['from-name']      = filter_input( INPUT_POST, 'from-name' );
			$config['reply-to-email'] = filter_input( INPUT_POST, 'reply-to-email' );
			$config['api-key']        = filter_input( INPUT_POST, 'api-key' );
			$config['list-id']        = filter_input( INPUT_POST, 'list-id' );
			$config['template-id']    = filter_input( INPUT_POST, 'template-id' );


			update_option( 'dm_mailchimp_settings', $config );
			$this->set_message( 'updated', 'Settings Saved' );
		}

		$this->settings = get_option( 'dm_mailchimp_settings' );
	}

	public function set_message( $class, $message ) {
		$this->message = '<div class=' . $class . '><p>' . $message . '</p></div>';
	}

	public function get_message() {
		return $this->message;
	}

}

/* Instantiate new class on plugins_loaded, best place to do this in MHO */
add_action( 'plugins_loaded', array( 'plugin_admin', 'get_instance' ) );