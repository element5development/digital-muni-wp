<?php
/*
Plugin Name: Digital Muni PDF Addition
Description: PDF Preview for IE problem
Plugin URI: URI goes here
Author: Digamber Pradhan
Author URI: https://www.digamberpradhan/
Version: 1.0.0
License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
Text Domain: dm-mailchimp-integration
*/

/**
 * Define Plugin FILE PATH
 */
if ( ! defined( 'DM_PDF_FILE_PATH' ) ) {
	define( 'DM_PDF_FILE_PATH', __FILE__ );
}
if ( ! defined( 'DM_PDF_DIR_PATH' ) ) {
	define( 'DM_PDF_DIR_PATH', dirname( __FILE__ ) );
}