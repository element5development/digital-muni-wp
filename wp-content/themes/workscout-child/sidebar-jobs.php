<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WorkScout
 */
$locreg_widget = $job_types_widget = $job_categories_widget = $salary_widget = $rate_widget = $job_tags_widget = 'on';

if ( is_page() && is_page_template( 'template-jobs.php' ) ) {
	$locreg_widget         = get_post_meta( $post->ID, 'pp_jobs_filters_locreg_widget', true );
	$job_types_widget      = get_post_meta( $post->ID, 'pp_jobs_filters_types_widget', true );
	$job_tags_widget       = get_post_meta( $post->ID, 'pp_jobs_filters_tags_widget', true );
	$job_categories_widget = get_post_meta( $post->ID, 'pp_jobs_filters_categories_widget', true );
	$salary_widget         = get_post_meta( $post->ID, 'pp_jobs_filters_salary_widget', true );
	$rate_widget           = get_post_meta( $post->ID, 'pp_jobs_filters_rate_widget', true );
}
?>
<!-- Widgets -->
<div id="dm-search-sidebar" class="advanced-search-popup zoom-anim-dialog apply-popup mfp-hide">
    <div id="dm-search-filters-container">
		<?php
		$search_in_sb = Kirki::get_option( 'workscout', 'pp_jobs_search_in_sb' );
		if ( $search_in_sb ) {
			if ( ! empty( $_GET['search_keywords'] ) ) {
				$keywords = sanitize_text_field( $_GET['search_keywords'] );
			} else {
				$keywords = '';
			}
			?>
            <div class="widget job-widget-keywords">
                <h4><?php esc_html_e( 'Keywords', 'workscout' ); ?></h4>
				<?php if ( is_page() && is_page_template( 'template-jobs.php' ) ) { ?>
                <form class="list-search" method="GET" action="<?php echo get_permalink(); ?>">
					<?php } else { ?>
                    <form class="list-search" method="GET" action="<?php echo get_permalink( get_option( 'job_manager_jobs_page_id' ) ); ?>">
						<?php } ?>
                        <div class="search_keywords">
                            <input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'job title, keywords or company name', 'workscout' ); ?>" value="<?php echo esc_attr( $keywords ); ?>"/>
                            <div class="clearfix"></div>
                        </div>
                    </form>
            </div>
		<?php } ?>
        <h3>Advanced Search</h3>
        <form class="job_filters in_sidebar">
			<?php
			if ( ! empty( $_GET['search_keywords'] ) ) {
				$keywords = sanitize_text_field( $_GET['search_keywords'] );
			} else {
				$keywords = '';
			}
			?>
            <input type="hidden" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'job title, keywords or company name', 'workscout' ); ?>" value="<?php echo esc_attr( $keywords ); ?>"/>

            <div class="job_filters_links"></div>
			<?php if ( get_query_var( 'company' ) ) { ?>
                <input type="hidden" name="company_field" value="<?php echo urldecode( get_query_var( 'company' ) ) ?>">
			<?php } ?>


			<?php /* if ( get_option( 'workscout_enable_location_sidebar' ) == 1 ) { ?>
			<?php if ( get_option( 'job_manager_regions_filter' ) || is_tax( 'job_listing_region' ) ) { ?>
                <div class="widget job-widget-regions" <?php if ( $locreg_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
                    <h4><?php esc_html_e( 'Region', 'workscout' ); ?></h4>
                    <div class="search_location">
						<?php

						if ( is_tax( 'job_listing_region' ) ) {
							$region   = get_query_var( 'job_listing_region' );
							$term     = get_term_by( 'slug', $region, 'job_listing_region' );
							$selected = $term->term_id;

						} else {
							$selected = isset( $_GET['search_region'] ) ? $_GET['search_region'] : '';
						}

						$dropdown       = wp_dropdown_categories( apply_filters( 'job_manager_regions_dropdown_args', array(
							'show_option_all' => __( 'All Regions', 'wp-job-manager-locations', 'workscout' ),
							'hierarchical'    => true,
							'orderby'         => 'name',
							'taxonomy'        => 'job_listing_region',
							'name'            => 'search_region',
							'id'              => 'search_location',
							'class'           => 'search_region job-manager-category-dropdown chosen-select-deselect ' . ( is_rtl() ? 'chosen-rtl' : '' ),
							'hide_empty'      => 1,
							'selected'        => $selected,
							'echo'            => false,
						) ) );
						$fixed_dropdown = str_replace( "&nbsp;", "", $dropdown );
						echo $fixed_dropdown;
						?>
                    </div>
                </div>
			<?php } else { ?>
                <div class="widget job-widget-location" <?php if ( $locreg_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
                    <h4><?php esc_html_e( 'Location', 'workscout' ); ?></h4>
                    <div class="search_location">
						<?php
						if ( ! empty( $_GET['search_location'] ) ) {
							$location = sanitize_text_field( $_GET['search_location'] );
						} else {
							$location = '';
						} ?>
                        <input type="text" name="search_location" id="search_location" placeholder="<?php esc_attr_e( 'Location', 'workscout' ); ?>" value="<?php echo esc_attr( $location ); ?>"/>

						<?php
						$map            = Kirki::get_option( 'workscout', 'pp_enable_jobs_map', 0 );
						$geocode        = Kirki::get_option( 'workscout', 'pp_maps_geocode', 0 );
						$default_radius = Kirki::get_option( 'workscout', 'pp_maps_default_radius', 0 );
						if ( $geocode ) : ?>
                            <input type="text" name="search_radius" id="search_radius" value="<?php echo esc_attr( $default_radius ) ?>"/>
                            <select class="radius_type chosen-select-radius" name="radius_type">
								<?php
								$miles_default = Kirki::get_option( 'workscout', 'pp_miles_default_map', 0 );
								if ( $miles_default ) { ?>
                                    <option value="miles"><?php esc_html_e( 'miles', 'workscout' ); ?></option>
                                    <option value="km"><?php esc_html_e( 'km', 'workscout' ); ?></option>
								<?php } else { ?>
                                    <option value="km"><?php esc_html_e( 'km', 'workscout' ); ?></option>
                                    <option value="miles"><?php esc_html_e( 'miles', 'workscout' ); ?></option>
								<?php } ?>
                            </select>
                            <div class="clearfix"></div>
						<?php endif; ?>
                    </div>


                </div>
			<?php } ?>
		<?php } */ ?>


			<?php /* if ( get_option( 'job_manager_enable_types' ) ) { ?>
            <div class="widget job-widget-job-types" <?php if ( $job_types_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
				<?php if ( ! is_tax( 'job_listing_type' ) ) : ?><h4><?php esc_html_e( 'Job type', 'workscout' ); ?></h4><?php endif; ?>
				<?php get_job_manager_template( 'job-filter-job-types.php', array( 'job_types' => '', 'atts' => array( 'orderby' => 'rand' ), 'selected_job_types' => '' ) ); ?>
            </div>
		<?php } */ ?>


			<?php
			/*if ( ! is_tax( 'job_listing_category' ) && get_terms( 'job_listing_category' ) ) :
				$show_category_multiselect = get_option( 'job_manager_enable_default_category_multiselect', false );

				if ( ! empty( $_GET['search_category'] ) ) {
					$selected_category = sanitize_text_field( $_GET['search_category'] );
				} else {
					$selected_category = "";
				}
				?>
				<div class="widget job-widget-categories" <?php if ( $job_categories_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
					<h4><?php esc_html_e( 'Category', 'workscout' ); ?></h4>
					<div class="search_categories">

						<?php if ( $show_category_multiselect ) : ?>
							<?php job_manager_dropdown_categories( array( 'taxonomy' => 'job_listing_category', 'hierarchical' => 1, 'depth' => - 1, 'name' => 'search_categories', 'orderby' => 'name', 'selected' => $selected_category, 'hide_empty' => false ) ); ?>
						<?php else : ?>
							<?php job_manager_dropdown_categories( array(
								'taxonomy'     => 'job_listing_category',
								'hierarchical' => 1,

								'show_option_all' => esc_html__( 'Any category', 'workscout' ),
								'name'            => 'search_categories',
								'orderby'         => 'name',
								'selected'        => $selected_category,
								'multiple'        => false,
								'hide_empty'      => false
							) ); ?>
						<?php endif; ?>

					</div>
				</div>
			<?php else: ?>
				<input type="hidden" name="search_categories[]" value="<?php echo sanitize_title( get_query_var( 'job_listing_category' ) ); ?>"/>
			<?php endif; ?>

			<?php if ( get_option( 'workscout_enable_filter_salary' ) ) : ?>
				<div class="widget widget_range_filter widget-salary-filter" <?php if ( $salary_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>

					<h4 class="checkboxes" style="margin-bottom: 0;">
						<input type="checkbox" name="filter_by_salary_check" id="salary_check" class="filter_by_check">
						<label for="salary_check"><?php esc_html_e( 'Filter by Salary', 'workscout' ); ?></label>
					</h4>

					<div class="widget_range_filter-inside">
						<div class="salary_amount range-indicator">
							<span class="from"></span> &mdash; <span class="to"></span>
						</div>
						<input type="hidden" name="filter_by_salary" id="salary_amount" type="checkbox">
						<div id="salary-range"></div>
						<div class="margin-bottom-50"></div>
					</div>

				</div>
			<?php endif; ?>

			<?php if ( get_option( 'workscout_enable_filter_rate' ) ) : ?>
				<div class="widget widget_range_filter widget-rate-filter" <?php if ( $rate_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
					<h4 class="checkboxes" style="margin-bottom: 0;">
						<input type="checkbox" name="filter_by_rate_check" id="filter_by_rate" class="filter_by_check">
						<label for="filter_by_rate"><?php esc_html_e( 'Filter by Rate', 'workscout' ); ?></label>
					</h4>
					<div class="widget_range_filter-inside">
						<div class="rate_amount range-indicator">
							<span class="from"></span> &mdash; <span class="to"></span>
						</div>
						<input type="hidden" name="filter_by_rate" id="rate_amount" type="checkbox">
						<div id="rate-range"></div>
					</div>
				</div>
			<?php endif; ?>

			<?php if ( taxonomy_exists( "job_listing_tag" ) && get_option( 'workscout_enable_job_tags_sidebar' ) ) { ?>
				<div class="widget widget_range_filter widget-tag" <?php if ( $job_tags_widget == "off" ) : echo ' style="display:none;" '; endif; ?>>
					<div class="filter_wide filter_by_tag">
						<h4><?php esc_html_e( 'Filter by tag:', 'workscout' ) ?></h4>
						<span class="filter_by_tag_cloud"></span>
					</div>
				</div>
			<?php }
	  ?>*/ ?>
            <div class="widget filter-options mfp-hide">
                <h4>Projects per page</h4>
                <select id="wsc-filter-perpage" name="wsc-perpage">
                    <option value="10">10 per page</option>
                    <option value="25">25 per page</option>
                    <option value="50">50 per page</option>
                    <option value="100">100 per page</option>
                </select>
            </div><!--filter-options-->
            <div class="dm-advanced-search-options">
                <div class="widget filter-options">
                    <h4>Filters</h4>
                    <div id="wsc-filter-keywords" class="dm-form-fields">
                        <label>
                            <input value="all" type="radio" name="wsc-filter-keywords">
                            <span class="radio"></span>
                            All Keywords
                        </label>
                        <label>
                            <input value="exact" type="radio" name="wsc-filter-keywords">
                            <span class="radio"></span>
                            Exact Keywords
                        </label>
                        <label>
                            <input value="any" type="radio" name="wsc-filter-keywords">
                            <span class="radio"></span>
                            Any Keywords
                        </label>
                    </div>
                </div><!--filter-options-->
                <div class="widget filter-results">
                    <h4>Filter Results</h4>
                    <div id="wsc-filter-doc-type" class="dm-form-fields">
                        <label>
                            <input value="both" type="radio" name="wsc-filter-doc-type">
                            <span class="radio"></span>
                            Both
                        </label>
                        <label>
                            <input value="preliminary" type="radio" name="wsc-filter-doc-type">
                            <span class="radio"></span>
                            Preliminary Only
                        </label>
                        <label>
                            <input value="final" type="radio" name="wsc-filter-doc-type">
                            <span class="radio"></span>
                            Final Only
                        </label>
                        <label>
                            <input value="other" type="radio" name="wsc-filter-doc-type">
                            <span class="radio"></span>
                            Other
                        </label>
                        <label>
                            <input value="investor-pres" type="radio" name="wsc-filter-doc-type">
                            <span class="radio"></span>
                            Investor Presentations
                        </label>
                    </div>
                </div><!--filter-options-->
                <div class="widget other-options">
                    <div class="fields-container">
                        <div class="dm-form-fields dm-full-col">
                            <label>Underwriter(S)</label>
				            <?php
				            if ( ! empty( $_GET['wsc-filter-underwriter'] ) ) {
					            $selected_category = sanitize_text_field( $_GET['wsc-filter-underwriter'] );
				            } else {
					            $selected_category = "";
				            }
				            job_manager_dropdown_categories( array(
					            'class'           => '',
					            'taxonomy'        => 'underwriter',
					            'hierarchical'    => 1,
					            'show_option_all' => esc_html__( 'Any underwriter', 'workscout' ),
					            'name'            => 'wsc-filter-underwriter',
					            'orderby'         => 'name',
					            'selected'        => $selected_category,
					            'multiple'        => false,
					            'hide_empty'      => false
				            ) ); ?>
                        </div>
                    </div>
                    <div class="fields-container">
                        <div class="dm-form-fields dm-full-col">
                            <label>Municipal Advisor(S)</label>
				            <?php
				            if ( ! empty( $_GET['wsc-filter-ma'] ) ) {
					            $selected_category = sanitize_text_field( $_GET['wsc-filter-ma'] );
				            } else {
					            $selected_category = "";
				            }
				            job_manager_dropdown_categories( array(
					            'class'           => '',
					            'taxonomy'        => 'ma',
					            'hierarchical'    => 1,
					            'show_option_all' => esc_html__( 'Any municipal advisor', 'workscout' ),
					            'name'            => 'wsc-filter-ma',
					            'orderby'         => 'name',
					            'selected'        => $selected_category,
					            'multiple'        => false,
					            'hide_empty'      => false
				            ) ); ?>
                        </div>
                    </div>
                </div>
                <div class="widget other-options">
                    <div class="fields-container">
                        <div class="dm-form-fields dm-half-col">
                            <label>State</label>
                            <select id="wsc-filter-state" name="wsc-state">
                                <option value="">--Select--</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            </select>
                        </div>
                        <div class="dm-form-fields dm-half-col">
                            <label>Timeframe</label>
                            <select id="wsc-filter-timeframe" name="wsc-filter-timeframe">
                                <option value=""></option>
                                <option value="month">Past month</option>
                                <option value="year">Past year</option>
                                <option value="week">Past week</option>
                                <option value="day">Past day</option>
                            </select>
                        </div>
                    </div>
                    <div class="fields-container">
                        <div class="dm-form-fields dm-half-col">
                            <label>Sort by</label>
                            <select id="wsc-sort-order-by" name="wsc-orderby">
                                <option value="date">By date</option>
                                <option value="title">Alphabetically</option>
                            </select>
                        </div>
                        <div class="dm-form-fields dm-half-col">
                            <label style="visibility:hidden;">Sort by</label>
                            <select id="wsc-sort-order" name="wsc-order">
                                <option value="DESC">Descending</option>
                                <option value="ASC">Ascending</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>


            <div class="buttons">
                <button id="wsc-submit-filters" type="submit" class="button">Apply</button>
            </div>

        </form>

		<?php dynamic_sidebar( 'sidebar-jobs' ); ?>
    </div>
</div><!-- #secondary -->
