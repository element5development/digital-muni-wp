<div class="dm-share-via-email">
    <div class="intro columns sixteen">
        <h3><?php the_title(); ?></h3>
        <div class="teaser">
			<?php the_field('bond_content'); ?>
        </div>
    </div>
    <form id="share-bond" method="post" action="">
		<?php wp_nonce_field( '_nonce_share_act', 'nonce_sharer' ); ?>
        <div class="eight columns">
            <p>
                <label for="share_kind">I'd like to</label>
                <select id="share_kind" name="share_kind">
                    <option value="self">Email to myself</option>
                    <option value="other">Email to someone else</option>
                </select>
            </p>
            <p class="share-mail-wrap">
                <label for="share_email">Email</label>
                <i>To send to multiple addresses, input emails separated by comma.</i>
                <input id="share_email" maxlength="255" name="share_email" type="text" value="">
            </p>
            <p>
                <label for="share_subject">Subject</label>
                <input id="share_subject" maxlength="255" name="share_subject" type="text" value="<?php the_title(); ?>">
            </p>
        </div>
        <div class="eight columns">
            <p class="share_content_wrap">
                <label for="share_content">Content</label>
                <textarea id="share_content" name="share_content"><?php echo strip_tags( get_field('bond_content') ); ?></textarea>
            </p>
        </div>
        <div class="sixteen  columns">
            <p>
                <input type="submit" value="Send" name="share_submit"/>
            </p>
        </div>
    </form>
</div>

<?php 
if ( isset( $_GET['success'] ) ) {?>
    <div id="mail-success" class="small-dialog zoom-anim-dialog apply-popup mfp-hide">
        <div class="small-dialog-content">
            <?php if ( 'true' == $_GET['success'] ) { ?>
                <h2>Mail sent successfully!!!</h2>
            <?php } elseif ( 'false' == $_GET['success'] ) { ?>
                <h2>Mail not sent. Try again!!!</h2>
            <?php } ?>
        </div>
    </div>
<?php } ?>
