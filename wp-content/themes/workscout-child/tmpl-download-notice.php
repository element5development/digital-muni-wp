<?php // Template Name: Download Notice and Disclaimer 

get_header();
if ( isset( $_GET['issue_id'] ) ) {
	$issue_id  = $_GET['issue_id'];
	$issue_obj = get_post( $issue_id );
	while (have_posts()) {
		the_post();
?>

<div id="titlebar" class="single">
	<div class="container">
		<div class="sixteen columns">
			<h2><?php the_title();?></h2>
			<span><?php the_field( 'bond_content', $issue_id ); ?></span>
		</div>

	</div>
</div>

<div class="container right-sidebar">
	<div class="eleven columns">
		<?php the_content(); ?>
		<label><input type="checkbox" name="download-notice-checkbox" class="download-notice-checkbox">
		I have read the foregoing disclaimer and expressly agree to each of its terms</label>
		
		<?php
		$link = add_query_arg( [
			'accepted'   => 'true',
			'project_id' => $issue_id
		], get_permalink( $issue_id ) ); ?>

		<a data-href="<?php echo esc_url( $link ); ?>" class="button disabled download-notice-btn" type="button" href="#">Accept</a>
		
	</div>
</div>
<?php
	}
}
get_footer();