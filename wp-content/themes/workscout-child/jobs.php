<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WPVoyager
 */

get_header();
?><!-- Titlebar
================================================== -->
<?php
$map          = Kirki::get_option( 'workscout', 'pp_enable_jobs_map', 0 );
$titlebar     = get_post_meta( $post->ID, 'pp_page_titlebar', true );
$header_image = get_post_meta( $post->ID, 'pp_job_header_bg', true );

if ( $titlebar == 'off' ) {
	// no titlebar
} else {
if ( ! empty( $header_image ) ) { ?>
<?php
$transparent_status = get_post_meta( $post->ID, 'pp_transparent_header', true );

if ( $transparent_status ){ ?>
    <div id="titlebar" class="photo-bg single with-transparent-header <?php if ( $map ) {
		echo " with-map";
	} ?>"" style="background: url('<?php echo esc_url( $header_image ); ?>')">
<?php } else { ?>
<div id="titlebar" class="photo-bg single <?php if ( $map ) {
	echo " with-map";
} ?>" style="background: url('<?php echo esc_url( $header_image ); ?>')">
	<?php } ?>
	<?php } else { ?>
    <div id="titlebar" class="single <?php if ( $map ) {
		echo " with-map";
	} ?>">
		<?php }
		$heading_text_color = get_field('heading_color','option');
		$qualified_text_color = get_field('qualified_under_color','option');
		?>
        <div class="container">
            <div class="sixteen columns">
                <div class="ten columns">
                    <h2 class="dm-title-heading" style="color:<?php echo $heading_text_color; ?>;">Recent Municipal Bond Offerings</h2>
                    <span class="dm-qualified"><a target="_blank" href="https://emma.msrb.org" style="color:<?php echo $qualified_text_color; ?>;">Qualified Portal Under MSRB Rule G-32</a></span>
                </div>
				<?php
				$call_to_action = Kirki::get_option( 'workscout', 'pp_call_to_action_jobs', 'job' );
				switch ( $call_to_action ) {
					case 'job':
						get_template_part( 'template-parts/button', 'job' );
						break;
					case 'resume':
						get_template_part( 'template-parts/button', 'resume' );
						break;
					default:
						# code...
						break;
				}
				?>
            </div>
        </div>
    </div>
	<?php
	}
	$layout = get_post_meta( $post->ID, 'pp_sidebar_layout', true );
	if ( empty( $layout ) ) {
		$layout = 'right-sidebar';
	}

	wp_dequeue_script( 'wp-job-manager-ajax-filters' );
	wp_enqueue_script( 'workscout-wp-job-manager-ajax-filters' );

	if ( $map ) {
		$all_map = Kirki::get_option( 'workscout', 'pp_enable_all_jobs_map', 0 );
		if ( $all_map ) {
			echo do_shortcode( '[workscout-map type="job_listing" class="jobs_page"]' );
		} else { ?>
            <div id="search_map"></div>
			<?php
		}
	} ?>

    <div class="container  wpjm-container <?php echo esc_attr( $layout ); ?>">
		<?php get_sidebar( 'jobs' ); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class( 'sixteen columns' ); ?>>
            <div class="padding-right">
				<?php
				$search_in_sb = Kirki::get_option( 'workscout', 'pp_jobs_search_in_sb' );
				if ( ! $search_in_sb ) {
					if ( ! empty( $_GET['search_keywords'] ) ) {
						$keywords = sanitize_text_field( $_GET['search_keywords'] );
					} else {
						$keywords = '';
					}
					?>
                    <form class="list-search" method="GET" action="<?php echo get_permalink( get_option( 'job_manager_jobs_page_id' ) ); ?>">
                        <div class="search_keywords">
                            <button><i class="fa fa-search"></i></button>
                            <input type="text" name="search_keywords" id="search_keywords" placeholder="<?php esc_attr_e( 'Issuer, CUSIP or any other term', 'workscout' ); ?>" value="<?php echo stripslashes( esc_attr( $keywords ) ); ?>" autocomplete="off"/>
                            <div class="clearfix"></div>
                        </div>
                    </form>

				<?php } ?>
				<div id="dm-filter-search" class="widget filter-options job_filters">

	                <div id="wsc-filter-mix-type" class="dm-form-fields">
                        <strong>Filter By</strong>
	                    <label>
	                        <input value="both" type="radio" name="wsc-filter-mix-type">
	                        <span class="radio"></span>
	                        All
	                    </label>
	                    <label>
	                        <input value="preliminary" type="radio" name="wsc-filter-mix-type">
	                        <span class="radio"></span>
	                        Preliminaries
	                    </label>
	                    <label>
	                        <input value="final" type="radio" name="wsc-filter-mix-type">
	                        <span class="radio"></span>
	                        Finals
	                    </label>
	                    <label>
	                        <input value="other" type="radio" name="wsc-filter-mix-type">
	                        <span class="radio"></span>
	                        Other
	                    </label>
	                    <label>
	                        <input value="investor-pres" type="radio" name="wsc-filter-mix-type">
	                        <span class="radio"></span>
	                    	Investor Presentations
	                    </label>
	                </div>
                    <div class="dm-filter-buttons">
                        <a href="#" id="wsc-reset-filters">Reset Filters</a>
                        <a href="#dm-search-sidebar" id="dm-show-advanced-search" class="dm-popup">Advanced Search</a>
                    </div>
	            </div><!--filter-options-->


                <!--<div id="mobile-search" class="hide-desktop">
                </div>-->

				<?php the_content(); ?>

		  		<div id="dm-filter-per-page-wrapper" class="widget filter-options">
		  		    <h4>Display</h4>
	                <ul id="wsc-filter-perpage-ul">
	                    <li><a href="#">10</a></li>
	                    <li><a href="#">25</a></li>
	                    <li><a href="#">50</a></li>
	                    <li><a href="#">100</a></li>
	                </ul>
		  		</div><!--filter-options-->

                <footer class="entry-footer">
					<?php edit_post_link( esc_html__( 'Edit', 'workscout' ), '<span class="edit-link">', '</span>' ); ?>
                </footer><!-- .entry-footer -->
            </div>
        </article>

    </div>
<?php
get_footer();
