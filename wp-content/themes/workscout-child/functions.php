<?php
add_action( 'wp_enqueue_scripts', 'workscout_enqueue_styles', 100 );
function workscout_enqueue_styles() {
	wp_register_style( 'parent-style', get_template_directory_uri() . '/style.css', array( 'workscout-base', 'workscout-responsive', 'workscout-font-awesome' ) );

	wp_register_style( 'montserrat', 'https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap', '', '', 'all' );
	wp_register_style( 'digitalmuni-main', get_stylesheet_directory_uri() . '/css/main.min.css', array( 'parent-style', 'montserrat' ), '1.5.5' );
	wp_enqueue_style( 'digitalmuni-main' );

	wp_enqueue_script( 'jquery-validate', get_stylesheet_directory_uri() . '/js/dev/jquery.validate.js', array( 'jquery' ), time(), true );

	wp_dequeue_script('workscout-wp-job-manager-ajax-filters' );
	wp_deregister_script('workscout-wp-job-manager-ajax-filters');
	global $wpdb;
	$ajax_url  = WP_Job_Manager_Ajax::get_endpoint();
	$min = 
    $wpdb->get_var("
            SELECT		min(meta_value + 0)
            FROM 		$wpdb->posts AS p
            LEFT JOIN 	$wpdb->postmeta AS m 
        				ON (p.ID = m.post_id)
            WHERE 		meta_key IN ('_salary_min','_salary_max')
            			AND meta_value != ''  
            			AND post_status = 'publish'
        "
    ) ;

	$max = ceil( $wpdb->get_var("
	    SELECT max(meta_value + 0)
	    FROM $wpdb->posts AS p
        LEFT JOIN $wpdb->postmeta AS m ON (p.ID = m.post_id)
	    WHERE meta_key IN ('_salary_min','_salary_max')  AND post_status = 'publish'
	"));

	$ratemin = floor( $wpdb->get_var("
            SELECT min(meta_value + 0)
            FROM $wpdb->posts AS p
        	LEFT JOIN $wpdb->postmeta AS m ON (p.ID = m.post_id)
            WHERE meta_key IN ('_rate_min')
            AND meta_value != ''  AND post_status = 'publish' AND post_type = 'job_listing'
       "));	

    $ratemax = ceil( $wpdb->get_var("
	    SELECT max(meta_value + 0)
	    FROM $wpdb->posts AS p
    	LEFT JOIN $wpdb->postmeta AS m ON (p.ID = m.post_id)
	    WHERE meta_key IN ('_rate_max')  AND post_status = 'publish' AND post_type = 'job_listing'
	") );

	wp_enqueue_script( 'workscout-wp-job-manager-ajax-filters-child', get_stylesheet_directory_uri() . '/js/workscout-ajax-filters.min.js', array( 'jquery' ), '1.0.0', true );
	wp_localize_script( 'workscout-wp-job-manager-ajax-filters-child', 'job_manager_ajax_filters', array(
				'ajax_url'                	=> $ajax_url,
				'is_rtl'                  	=> is_rtl() ? 1 : 0,
				'lang'                    	=> defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : '', // WPML workaround until this is standardized
				'i18n_load_prev_listings' 	=> esc_html__( 'Load previous listings', 'workscout' ),
				'salary_min'		      	=> $min,
				'salary_max'		      	=> $max,
				'rate_min'		      		=> $ratemin,
				'rate_max'		      		=> $ratemax,
				'single_job_text'			=> esc_html__('job offer','workscout'),
				'plural_job_text'			=> esc_html__('job offers','workscout'),
				'currency'		      		=> get_workscout_currency_symbol(),
				'currency_postion'     		=> get_option('workscout_currency_position','before'),

			) );

	wp_enqueue_script( 'workscout-parent-custom', get_stylesheet_directory_uri() . '/js/dev/custom.js', array('jquery'), '1.2.0', true );
	$ajax_url = admin_url( 'admin-ajax.php', 'relative' );

	wp_localize_script( 'workscout-parent-custom', 'ws',
    array(
        'logo'					=> Kirki::get_option( 'workscout','pp_logo_upload', ''),
        'retinalogo'			=> Kirki::get_option( 'workscout','pp_retina_logo_upload',''),
        'transparentlogo'		=> Kirki::get_option( 'workscout','pp_transparent_logo_upload', ''),
        'transparentretinalogo'	=> Kirki::get_option( 'workscout','pp_transparent_retina_logo_upload',''),
        'ajaxurl' 				=> $ajax_url,
        'theme_color' 			=> Kirki::get_option( 'workscout', 'pp_main_color' ),
        'woo_account_page'		=> get_permalink(get_option('woocommerce_myaccount_page_id')),
        'theme_url'				=> get_template_directory_uri(),
        'header_breakpoint'		=> Kirki::get_option( 'workscout','pp_alt_menu_width','1290'),
       	'no_results_text'		=> __('No results match','workscout'),
        )
    );

	wp_enqueue_script( 'workscout-child-custom', get_stylesheet_directory_uri() . '/js/dev/child-custom.js', array( 'jquery', 'jquery-validate', 'workscout-parent-custom' ), '1.3.2', true );
	wp_localize_script( 'workscout-child-custom', 'dm_data', [ 'ajax_url' => admin_url( 'admin-ajax.php' ) ] );

}

function workscout_child_dequeue_script() {
	wp_dequeue_script( 'workscout-custom' );
	wp_deregister_script( 'workscout-custom' );
}
add_action( 'wp_print_scripts', 'workscout_child_dequeue_script', 100 );

require get_stylesheet_directory() . '/inc/helper-functions.php';

require get_stylesheet_directory() . '/inc/template-redirect.php';

require get_stylesheet_directory() . '/inc/post-types.php';

require get_stylesheet_directory() . '/inc/taxonomy.php';

require get_stylesheet_directory() . '/inc/single-project-meta.php';

require_once get_stylesheet_directory() . '/inc/filter-jobs.php';

require_once get_stylesheet_directory() . '/inc/role.php';

require_once get_stylesheet_directory() . '/inc/dm-options.php';

require_once get_stylesheet_directory() . '/inc/ajax-callback.php';

require_once get_stylesheet_directory() . '/inc/admin-mods.php';