<?php global $post;
$desktop_embed_code = get_field( 'presentation_embed_code' );
$mobile_embed_code  = get_field( 'presentation_embed_code_mobile' );

$position        = Kirki::get_option( 'workscout', 'pp_job_list_logo_position', 'left' );
$layout          = Kirki::get_option( 'workscout', 'pp_jobs_old_layout', false );
$_color_job_type = Kirki::get_option( 'workscout', 'pp_maps_marker_color_job_type', false );
if ( get_option( 'job_manager_enable_types' ) ) {
	$types = get_the_terms( $post->ID, 'job_listing_type' );
	if ( $types && ! is_wp_error( $types ) && $_color_job_type ) :
		$single_type = $types[0];
		$markercolor = get_term_meta( $single_type->term_id, 'color', true );
	endif;
}
if ( ! $layout ) {

	?>
    <!-- Listing -->
    <li class="dm-list-item" data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" <?php if ( isset( $markercolor ) ) {
		echo 'data-color="#' . $markercolor . '"';
	} ?>>

        <a href="<?php the_job_permalink(); ?>" <?php job_listing_class( $position ); ?> data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>">
			<?php /*
    <div class="listing-logo">
		<?php ($position == 'left') ? the_company_logo() : the_company_logo('medium'); ?>
	</div> */ ?>
            <div class="listing-title">
                <h4><?php the_title(); ?></h4>
                <ul class="listing-icons">
					<?php do_action( 'workscout_job_listing_meta_start' ); ?>

					<?php $job_meta = Kirki::get_option( 'workscout', 'pp_meta_job_list', array( 'company', 'location', 'rate', 'salary' ) ); ?>

					<?php if ( ! empty( get_field( 'state_of_issuer' ) ) ) { ?>
                        <li><i class="ln dm-icon-marker"></i> <?php the_field( 'state_of_issuer' ); ?></li>
					<?php } ?>
                    <li><i class="ln dm-icon-calendar"></i> <?php echo get_the_date( get_option( 'date_format' ) ); ?> <?php the_time( get_option( 'time_format' ) ); ?></li>
					
					<?php 
					$document_type_obj = get_field( 'type_of_document' );

					if ( ! empty( $document_type_obj ) ) {
					?>
	                    <p style="color:#<?php echo get_term_meta( $document_type_obj->term_id, 'color', true );?>" class="document_type">
	                    	<?php echo $document_type_obj->name; ?>
						</p>
					<?php } ?>
                    <div class="listing-desc"><?php the_field( 'bond_content' ); ?></div>

                </ul>
            </div><!--listing-title-->
        </a>
        <div class="buttons">
            <button href="#" class="open-download-notice-popup button"><?php $button_text = get_field( 'pdf_button_text_listing_page', 'option' );
				echo ! empty( $button_text ) ? $button_text : 'Download';
				?></button>
            <button href="#" class="open-mini-info-popup button"><?php esc_html_e( 'Muni Info', 'workscout' ) ?></button>
			<?php if ( ! empty( $desktop_embed_code ) ): ?>
                <a href="#" class="open-presentation-popup" title="open presentation" data-project_id="<?php echo get_the_ID(); ?>">
                    <i class="fa fa-video-camera"></i>
                </a>
			<?php endif; ?>
        </div>

        <!-- Add popup HTML structure for download notice disclaimer -->
        <div id="download-notice" class="small-dialog zoom-anim-dialog apply-popup mfp-hide">
            <div class="small-dialog-content">
                <h3>Download Notice and Disclaimer</h3>
                <div class="highlight-text"><?php the_field( 'bond_content' ); ?></div>
                <h3>Disclaimer</h3>
                <div class="disclaimer">
                    <p>The information provided by DigitalMuni.com is for informational purposes only and does not constitute an offer to sell nor is it a solicitation of an offer to buy securities. Any offer or solicitation is subject to registration or qualification under the securities laws of the appropriate jurisdiction.</p>
                    <p>
                        This Official Statement was created using Adobe Acrobat Portable Document Format (PDF) and best displayed using the most current version of Adobe Acrobat or Acrobat Reader. If you do not have Adobe Acrobat Reader, a free copy is available for download.
                    </p>
                    <p>
                        By choosing to download and view this electronic Official Statement, you are acknowledging that you have read and understand this Notice and Disclaimer.
                    </p>

                    <p>Please call <a href="tel:248-237-4077">248-237-4077</a> if you require assistance with downloading this document.</p>
                </div>

                <label><input type="checkbox" name="download-notice-checkbox" class="download-notice-checkbox"><span class="checkbox"></span>
                    I have read the foregoing disclaimer and expressly agree to each of its terms</label>

                <a data-href="<?php echo esc_url( get_permalink( $post->ID ) . '?accepted=true&project_id=' . $post->ID ); ?>" class="button disabled download-notice-btn" type="button" href="#">Accept</a>
            </div>
        </div>
        <!-- End popup HTML structure for download notice disclaimer -->

        <div id="mini-info" class="small-dialog zoom-anim-dialog apply-popup mfp-hide">
            <div class="small-dialog-content">
                <div class="bond-attr">
                    <h3><?php the_title(); ?></h3>
                    <p><?php the_field( 'bond_content' ); ?></p>
                </div>

				<?php if ( ! empty( get_field( 'ratings' ) ) ) { ?>
                    <div class="bond-attr rating">
                        <h3>Ratings</h3>
						<?php
						$ratings = get_field( 'ratings' );
						$ratings = explode( ',', $ratings );
						$matches = [];
						if ( ! empty( $ratings ) ) {
							foreach ( $ratings as $rating ) {
								$rating = trim( $rating );
								preg_match( '/[^:]*/', $rating, $matches );
								$rating = str_replace( $matches, '<strong>' . $matches[0] . '</strong>', $rating );
								echo str_replace( ':', ' ', $rating ) . '<br />';
							}
						} ?>
                    </div>
				<?php } ?>

				<?php
				$underwriters_obj = get_field( 'underwriters' );
				if ( ! empty( $underwriters_obj ) ) { ?>
                    <div class="bond-attr underwriters">
                        <h3>Underwriter(s)</h3>
                        <p>
							<?php
							foreach ( $underwriters_obj as $underwriters ) {
								$underwriters_name[] = $underwriters->name;
							}
							echo implode( ", ", $underwriters_name );
							?>

                        </p>
                    </div>
				<?php } ?>

				<?php
				$mas_obj = get_field( 'mas' );
				if ( ! empty( $mas_obj ) ) { ?>
                    <div class="bond-attr MAs">
                        <h3>Municipal Advisor(s)</h3>
                        <p>
							<?php
							foreach ( $mas_obj as $mas ) {
								$mas_name[] = $mas->name;
							}
							echo implode( ", ", $mas_name );
							?>
                        </p>
                    </div>
				<?php } ?>

				<?php if ( ! empty( get_field( 'bond_amount' ) ) ) { ?>
                    <div class="bond-attr dollar-value">
                        <h3>Amount</h3>
                        <p>$<?php the_field( 'bond_amount' ); ?></p>
                    </div>
				<?php } ?>

            </div>
        </div>

    </li>
	<?php
} else { ?>


    <li <?php job_listing_class( $position ); ?> data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>" <?php if ( isset( $markercolor ) ) {
		echo 'data-color="#' . $markercolor . '"';
	} ?>>
        <a href="<?php the_job_permalink(); ?>">
			<?php

			( $position == 'left' ) ? the_company_logo() : the_company_logo( 'medium' ); ?>
            <div class="job-list-content">
                <h4><?php the_title(); ?>
					<?php /*if ( get_option( 'job_manager_enable_types' ) ) {
						$types = get_the_terms( $post->ID, 'job_listing_type' );
						if ( $types && ! is_wp_error( $types ) ) :
							foreach ( $types as $type ) { ?>
                                <span class="job-type <?php echo sanitize_title( $type->slug ); ?>"><?php echo $type->name; ?></span>
							<?php }
						endif; ?>
					<?php } ?>
					<?php if ( workscout_newly_posted() ) {
						echo '<span class="new_job">' . esc_html__( 'NEW', 'workscout' ) . '</span>';
					} */ ?>
                </h4>

                <div class="job-icons">
					<?php do_action( 'workscout_job_listing_meta_start' ); ?>

					<?php $job_meta = Kirki::get_option( 'workscout', 'pp_meta_job_list', array( 'company', 'location', 'rate', 'salary' ) ); ?>

					<?php if ( in_array( "company", $job_meta ) && get_the_company_name() ) { ?>
                        <span class="ws-meta-company-name"><i class="fa fa-briefcase"></i> <?php the_company_name(); ?></span>
					<?php } ?>

					<?php if ( in_array( "location", $job_meta ) ) { ?>
                        <span class="ws-meta-job-location"><i class="fa fa-map-marker"></i> <?php ws_job_location( false ); ?></span>
					<?php } ?>

					<?php
					$currency_position = get_option( 'workscout_currency_position', 'before' );

					$rate_min = get_post_meta( $post->ID, '_rate_min', true );
					if ( $rate_min && in_array( "rate", $job_meta ) ) {
						$rate_max = get_post_meta( $post->ID, '_rate_max', true ); ?>
                        <span class="ws-meta-rate">
						<i class="fa fa-money"></i> <?php
							if ( $currency_position == 'before' ) {
								echo get_workscout_currency_symbol();
							}
							echo esc_html( $rate_min );
							if ( $currency_position == 'after' ) {
								echo get_workscout_currency_symbol();
							}
							if ( ! empty( $rate_max ) ) {
								echo '- ';
								if ( $currency_position == 'before' ) {
									echo get_workscout_currency_symbol();
								}
								echo $rate_max;
								if ( $currency_position == 'after' ) {
									echo get_workscout_currency_symbol();
								}
							} ?> <?php esc_html_e( '/ hour', 'workscout' ); ?>
					</span>
					<?php } ?>

					<?php
					$salary_min = get_post_meta( $post->ID, '_salary_min', true );
					$salary_max = get_post_meta( $post->ID, '_salary_max', true );
					if ( in_array( "salary", $job_meta ) ) :
						if ( ! empty( $salary_min ) || ! empty( $salary_max ) ) { ?>
                            <span class="ws-meta-salary">
							<i class="fa fa-money"></i>
								<?php
								if ( $salary_min ) {
									if ( $currency_position == 'before' ) {
										echo get_workscout_currency_symbol();
									}
									echo esc_html( $salary_min );
									if ( $currency_position == 'after' ) {
										echo get_workscout_currency_symbol();
									}
								}
								if ( $salary_max ) {
									if ( $salary_min ) {
										echo ' - ';
									}
									if ( $currency_position == 'before' ) {
										echo get_workscout_currency_symbol();
									}
									echo $salary_max;
									if ( $currency_position == 'after' ) {
										echo get_workscout_currency_symbol();
									}
								} ?>
						</span>
						<?php }
					endif; ?>

					<?php if ( in_array( "date", $job_meta ) ) { ?>
                        <span class="ws-meta-job-date"><i class="fa fa-calendar"></i> <?php the_job_publish_date() ?></span>
					<?php } ?>

					<?php if ( in_array( "deadline", $job_meta ) ) { ?>
						<?php
						if ( $deadline = get_post_meta( $post->ID, '_application_deadline', true ) ) {
							$expiring_days = apply_filters( 'job_manager_application_deadline_expiring_days', 2 );
							$expiring      = ( floor( ( time() - strtotime( $deadline ) ) / ( 60 * 60 * 24 ) ) >= $expiring_days );
							$expired       = ( floor( ( time() - strtotime( $deadline ) ) / ( 60 * 60 * 24 ) ) >= 0 ); ?>
                            <span class="ws-meta-job-deadline"><i class="fa fa-calendar-times-o"></i>
								<?php echo ( $expired ? __( 'Closed', 'workscout' ) : __( 'Closes', 'workscout' ) ) . ': ' . date_i18n( get_option( 'date_format' ), strtotime( $deadline ) ) ?>
						</span>
						<?php }
					} ?>

					<?php if ( in_array( "expires", $job_meta ) ) { ?>
                        <span class="ws-meta-job-expires"><i class="fa fa-calendar-check-o"></i>
							<?php esc_html_e( 'Expires', 'workscout' ) ?>: <?php echo date_i18n( get_option( 'date_format' ), strtotime( get_post_meta( $post->ID, '_job_expires', true ) ) ) ?>
					</span>
					<?php } ?>

					<?php do_action( 'workscout_job_listing_meta_end' ); ?>
                </div>
                <div class="listing-desc"><?php the_excerpt(); ?>

                </div>


            </div>
        </a>
        <div class="clearfix"></div>
    </li>

<?php } ?>
