<?php
// Redirect user in case of accessing the single issue directly.
function workscout_child_custom_user_redirect( $redirect, $user ) {
	if ( ! is_user_logged_in() && isset( $_GET['accepted'] ) && isset( $_GET['project_id'] ) ) {
		$permalink = get_permalink( $_GET['project_id'] );
		$redirect      = add_query_arg( [
		    'accepted'   => 'true',
		    'project_id' => $_GET['project_id']
		], $permalink );
	}
	
	return $redirect;
}
add_filter( 'woocommerce_login_redirect', 'workscout_child_custom_user_redirect', 20, 2 );

add_action( 'template_redirect', function () {
	// Check if viewing the single issue directly.
	if ( is_singular( 'job_listing' ) ) {

		if ( isset( $_GET['accepted'] ) && isset( $_GET['show_pdf'] ) && isset( $_GET['project_id'] ) ) {
			add_filter( 'template_include', function ( $template ) {
				$template = locate_template( 'templates/tpl-show-pdf.php' );

				return $template;
			} );
		} elseif ( ! is_user_logged_in() && isset( $_GET['accepted'] ) && isset( $_GET['project_id'] ) ) {
			$permalink = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );
			$link      = add_query_arg( [
			    'accepted'   => 'true',
			    'project_id' => $_GET['project_id']
			], $permalink );
			wp_redirect( $link, 307 );
			exit();
		} elseif ( is_user_logged_in() && ! isset( $_GET['accepted'] ) ) {
			$page_id = get_page_by_path( 'download-notice-and-disclaimer' );
			wp_redirect( get_permalink( $page_id ) . '?issue_id=' . get_the_ID(), 307 );
			exit();
		}
	}

	// Send mail functionality and redirect user to homepage on sending the mail.
	if ( isset( $_POST['share_submit'] ) && isset( $_POST['nonce_sharer'] ) ) {
		list( $nonce_sharer, $_wp_http_referer, $share_kind, $share_email, $share_subject, $share_content ) = array_values( $_POST );

		if ( 'self' == $share_kind ) {
			$email = wp_get_current_user()->user_email;
		} else {
			$email = $share_email;
		}

		global $post;
		$pdf_url_arr = get_field( 'upload_document', $post->ID );
		if ( ! empty( $pdf_url_arr ) ) {
			$pdf_url       = $pdf_url_arr['url'];
			$share_content .= '<strong><a target="_blank" href="' . esc_url( $pdf_url ) . '">Download here.</a></strong>';
		}

		// $pdf_url = $pdf_url_arr['url'];
		// $pdf_url = !empty($pdf_url) ? $pdf_url : false;
		$headers = array( 'Content-Type: text/html; charset=UTF-8' );

		if ( wp_mail( $email, $share_subject, $share_content, $headers ) ) {
			wp_redirect( get_permalink() . '?success=true&accepted&share' );
		} else {
			wp_redirect( get_permalink() . '?success=false&accepted&share' );
		}
	}

} );

// Function to change email address
function workscout_child_change_sender_email( $original_email_address ) {
	return 'mail@digitamuni.com';
}

// Function to change sender name
function workscout_child_change_sender_name( $original_email_from ) {
	global $post;
	$title = get_the_title( $post );

	return 'Digital Muni ' . esc_html( $title );
}

// Hooking up our functions to WordPress filters 
add_filter( 'wp_mail_from', 'workscout_child_change_sender_email' );
add_filter( 'wp_mail_from_name', 'workscout_child_change_sender_name' );
