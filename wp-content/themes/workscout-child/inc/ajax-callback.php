<?php
function dm_get_embedded_pdf() {
	$project_id = filter_input( INPUT_POST, 'project_id' );
	if ( ! $project_id ) {
		wp_send_json_error( 'Error: Project ID Not Provided' );
	}

	$desktop_embed_code = get_field( 'presentation_embed_code', $project_id );
	$mobile_embed_code  = get_field( 'presentation_embed_code_mobile', $project_id );
	$data               = [
		'pdf_desktop' => $desktop_embed_code,
		'pdf_mobile'  => $mobile_embed_code
	];

	wp_send_json_success( $data );
}

//$action = 'dm_get_embedded_pdf';
add_action( 'wp_ajax_nopriv_dm_get_embedded_pdf', 'dm_get_embedded_pdf' );
add_action( 'wp_ajax_dm_get_embedded_pdf', 'dm_get_embedded_pdf' );