<?php
add_action( 'editable_roles' , 'hide_adminstrator_editable_roles' );
function hide_adminstrator_editable_roles( $roles ){
    unset( $roles['shop_manager'] );
    unset( $roles['employer'] );
    unset( $roles['subscriber'] );
    unset( $roles['contributor'] );
    unset( $roles['author'] );
    unset( $roles['editor'] );
    return $roles;
}

function wsc_role_exists( $role ) {

	if( ! empty( $role ) ) {
		return $GLOBALS['wp_roles']->is_role( $role );
	}

	return false;
}

add_action( 'admin_init', 'workscout_child_add_new_role' );

function workscout_child_add_new_role() {

	if ( ! wsc_role_exists( 'wsc_project_editor' ) ) {

		add_role(
			'wsc_project_editor',
		    __( 'Project Editor' ),
		    array(
    			'read' => true, 
				'edit_job_listing'   => true, 
				'edit_job_listings' => true,
				'edit_published_job_listings' => false,
			)
		);
	}

	if ( ! wsc_role_exists( 'wsc_project_admin' ) ) {
		add_role(
			'wsc_project_admin',
		    __( 'Project Admin' ),
		    array(
    			'read' => true, 
				'edit_job_listing'   => true, 
				'edit_job_listings' => true,
				'edit_published_job_listings' => true,
				'edit_others_job_listings' => true,
				'publish_job_listings' => true,
				'delete_job_listings' => true,
				'delete_others_job_listings' => true,
				'delete_published_job_listings' => true,
				'delete_private_job_listings' => true,
				'delete_private_job_listings' => true,
				'list_users' => true,
			)
		);
	}

	// // Add the roles you'd like to administer the custom post types
	// $roles = array('wsc_editor');

	// // Loop through each role and assign capabilities
	// foreach($roles as $the_role) { 
	     // $role = get_role('wsc_project_admin');
	     $role = get_role('administrator');
	     // $role->add_cap( 'edit_others_job_listings', true );
	     // $role->add_cap( 'read_private_job_listings', false );
	     $role->add_cap( 'edit_user_downloads', true );
	     $role->add_cap( 'edit_others_user_downloads', true );
	     $role->add_cap( 'publish_user_downloads', true );
	     $role->add_cap( 'read_private_user_downloads', true );

	     $wsc_role = get_role('wsc_project_admin');
	     $wsc_role->add_cap( 'edit_user_downloads', true );
	     $wsc_role->add_cap( 'edit_others_user_downloads', true );
	     $wsc_role->add_cap( 'publish_user_downloads', true );
	     $wsc_role->add_cap( 'read_private_user_downloads', true );

	// }
}

$user = wp_get_current_user();
if ( in_array( 'wsc_project_editor', (array) $user->roles ) || in_array( 'wsc_project_admin', (array) $user->roles ) ) {
    //The user has the "author" role
	add_filter( 'woocommerce_disable_admin_bar', '__return_false' );
}

function workscout_child_remove_items_my_account( $items ) {
	unset( $items['edit-address'] );
	unset( $items['orders'] );
	return $items;
}
add_filter( 'woocommerce_account_menu_items', 'workscout_child_remove_items_my_account', 999 );