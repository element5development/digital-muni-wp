<?php
/**
Register extra taxonomy as required. Will be used in advanced search.

*/


/**
 * Registers the `underwriter` taxonomy,
 * for use with 'job_listing'.
 */
function workscout_child_underwriter_init() {
	register_taxonomy( 'underwriter', array( 'job_listing' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'meta_box_cb'		=> false,
		'capabilities'      => array(
			'manage_terms'  => 'manage_uw_terms',
			'edit_terms'    => 'manage_uw_terms',
			'delete_terms'  => 'manage_uw_terms',
			'assign_terms'  => 'manage_uw_terms',
		),
		'labels'            => array(
			'name'                       => __( 'Underwriters', 'workscout-child' ),
			'singular_name'              => _x( 'Underwriter', 'taxonomy general name', 'workscout-child' ),
			'search_items'               => __( 'Search Underwriters', 'workscout-child' ),
			'popular_items'              => __( 'Popular Underwriters', 'workscout-child' ),
			'all_items'                  => __( 'All Underwriters', 'workscout-child' ),
			'parent_item'                => __( 'Parent Underwriter', 'workscout-child' ),
			'parent_item_colon'          => __( 'Parent Underwriter:', 'workscout-child' ),
			'edit_item'                  => __( 'Edit Underwriter', 'workscout-child' ),
			'update_item'                => __( 'Update Underwriter', 'workscout-child' ),
			'view_item'                  => __( 'View Underwriter', 'workscout-child' ),
			'add_new_item'               => __( 'Add New Underwriter', 'workscout-child' ),
			'new_item_name'              => __( 'New Underwriter', 'workscout-child' ),
			'separate_items_with_commas' => __( 'Separate underwriters with commas', 'workscout-child' ),
			'add_or_remove_items'        => __( 'Add or remove underwriters', 'workscout-child' ),
			'choose_from_most_used'      => __( 'Choose from the most used underwriters', 'workscout-child' ),
			'not_found'                  => __( 'No underwriters found.', 'workscout-child' ),
			'no_terms'                   => __( 'No underwriters', 'workscout-child' ),
			'menu_name'                  => __( 'Underwriters', 'workscout-child' ),
			'items_list_navigation'      => __( 'Underwriters list navigation', 'workscout-child' ),
			'items_list'                 => __( 'Underwriters list', 'workscout-child' ),
			'most_used'                  => _x( 'Most Used', 'underwriter', 'workscout-child' ),
			'back_to_items'              => __( '&larr; Back to Underwriters', 'workscout-child' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'underwriter',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'workscout_child_underwriter_init' );

/**
 * Sets the post updated messages for the `underwriter` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `underwriter` taxonomy.
 */
function workscout_child_underwriter_updated_messages( $messages ) {

	$messages['underwriter'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Underwriter added.', 'workscout-child' ),
		2 => __( 'Underwriter deleted.', 'workscout-child' ),
		3 => __( 'Underwriter updated.', 'workscout-child' ),
		4 => __( 'Underwriter not added.', 'workscout-child' ),
		5 => __( 'Underwriter not updated.', 'workscout-child' ),
		6 => __( 'Underwriters deleted.', 'workscout-child' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'workscout_child_underwriter_updated_messages' );

/**
 * Registers the `ma` taxonomy,
 * for use with 'job_listing'.
 */
function workscout_child_ma_init() {
	register_taxonomy( 'ma', array( 'job_listing' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'meta_box_cb'		=> false,
		'capabilities'      => array(
			'manage_terms'  => 'manage_ma_terms',
			'edit_terms'    => 'manage_ma_terms',
			'delete_terms'  => 'manage_ma_terms',
			'assign_terms'  => 'manage_ma_terms',
		),
		'labels'            => array(
			'name'                       => __( 'Municipal Advisors', 'workscout-child' ),
			'singular_name'              => _x( 'Municipal Advisor', 'taxonomy general name', 'workscout-child' ),
			'search_items'               => __( 'Search Municipal Advisors', 'workscout-child' ),
			'popular_items'              => __( 'Popular Municipal Advisors', 'workscout-child' ),
			'all_items'                  => __( 'All Municipal Advisors', 'workscout-child' ),
			'parent_item'                => __( 'Parent Municipal Advisor', 'workscout-child' ),
			'parent_item_colon'          => __( 'Parent Municipal Advisor:', 'workscout-child' ),
			'edit_item'                  => __( 'Edit Municipal Advisor', 'workscout-child' ),
			'update_item'                => __( 'Update Municipal Advisor', 'workscout-child' ),
			'view_item'                  => __( 'View Municipal Advisor', 'workscout-child' ),
			'add_new_item'               => __( 'Add New Municipal Advisor', 'workscout-child' ),
			'new_item_name'              => __( 'New Municipal Advisor', 'workscout-child' ),
			'separate_items_with_commas' => __( 'Separate Municipal Advisors with commas', 'workscout-child' ),
			'add_or_remove_items'        => __( 'Add or remove Municipal Advisors', 'workscout-child' ),
			'choose_from_most_used'      => __( 'Choose from the most used Municipal Advisors', 'workscout-child' ),
			'not_found'                  => __( 'No Municipal Advisors found.', 'workscout-child' ),
			'no_terms'                   => __( 'No Municipal Advisors', 'workscout-child' ),
			'menu_name'                  => __( 'Municipal Advisors', 'workscout-child' ),
			'items_list_navigation'      => __( 'Municipal Advisors list navigation', 'workscout-child' ),
			'items_list'                 => __( 'Municipal Advisors list', 'workscout-child' ),
			'most_used'                  => _x( 'Most Used', 'Municipal Advisor', 'workscout-child' ),
			'back_to_items'              => __( '&larr; Back to Municipal Advisors', 'workscout-child' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'ma',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'workscout_child_ma_init' );

/**
 * Sets the post updated messages for the `ma` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `ma` taxonomy.
 */
function workscout_child_ma_updated_messages( $messages ) {

	$messages['ma'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'MA added.', 'workscout-child' ),
		2 => __( 'MA deleted.', 'workscout-child' ),
		3 => __( 'MA updated.', 'workscout-child' ),
		4 => __( 'MA not added.', 'workscout-child' ),
		5 => __( 'MA not updated.', 'workscout-child' ),
		6 => __( 'MAs deleted.', 'workscout-child' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'workscout_child_ma_updated_messages' );

add_filter( 'register_taxonomy_job_listing_type_args', 'workscout_child_rename_job_type' );
function workscout_child_rename_job_type( $args ) {
	$args['meta_box_cb'] = false;
	$args['labels']            = array(
		'name'                       => __( 'Type of document', 'workscout-child' ),
		'singular_name'              => _x( 'Type of document', 'taxonomy general name', 'workscout-child' ),
		'search_items'               => __( 'Search type of documents', 'workscout-child' ),
		'popular_items'              => __( 'Popular type of documents', 'workscout-child' ),
		'all_items'                  => __( 'All type of documents', 'workscout-child' ),
		'parent_item'                => __( 'Parent type of document', 'workscout-child' ),
		'parent_item_colon'          => __( 'Parent type of document:', 'workscout-child' ),
		'edit_item'                  => __( 'Edit type of document', 'workscout-child' ),
		'update_item'                => __( 'Update type of document', 'workscout-child' ),
		'view_item'                  => __( 'View type of document', 'workscout-child' ),
		'add_new_item'               => __( 'Add New type of document', 'workscout-child' ),
		'new_item_name'              => __( 'New type of document', 'workscout-child' ),
		'separate_items_with_commas' => __( 'Separate type of documents with commas', 'workscout-child' ),
		'add_or_remove_items'        => __( 'Add or remove type of documents', 'workscout-child' ),
		'choose_from_most_used'      => __( 'Choose from the most used type of documents', 'workscout-child' ),
		'not_found'                  => __( 'No type of documents found.', 'workscout-child' ),
		'no_terms'                   => __( 'No type of documents', 'workscout-child' ),
		'menu_name'                  => __( 'Type of documents', 'workscout-child' ),
		'items_list_navigation'      => __( 'Type of documents list navigation', 'workscout-child' ),
		'items_list'                 => __( 'Type of documents list', 'workscout-child' ),
		'most_used'                  => _x( 'Most Used', 'type of document', 'workscout-child' ),
		'back_to_items'              => __( '&larr; Back to type of documents', 'workscout-child' ),
	);
	return $args;
}

add_filter( 'register_taxonomy_job_listing_category_args', 'workscout_child_rename_job_cat' );
function workscout_child_rename_job_cat( $args ) {
	$args['meta_box_cb'] = false;
	$args['labels']            = array(
		'name'                       => __( 'Bond sale', 'workscout-child' ),
		'singular_name'              => _x( 'Bond sale', 'taxonomy general name', 'workscout-child' ),
		'search_items'               => __( 'Search bond sales', 'workscout-child' ),
		'popular_items'              => __( 'Popular bond sales', 'workscout-child' ),
		'all_items'                  => __( 'All bond sales', 'workscout-child' ),
		'parent_item'                => __( 'Parent bond sale', 'workscout-child' ),
		'parent_item_colon'          => __( 'Parent bond sale:', 'workscout-child' ),
		'edit_item'                  => __( 'Edit bond sale', 'workscout-child' ),
		'update_item'                => __( 'Update bond sale', 'workscout-child' ),
		'view_item'                  => __( 'View bond sale', 'workscout-child' ),
		'add_new_item'               => __( 'Add New bond sale', 'workscout-child' ),
		'new_item_name'              => __( 'New bond sale', 'workscout-child' ),
		'separate_items_with_commas' => __( 'Separate bond sales with commas', 'workscout-child' ),
		'add_or_remove_items'        => __( 'Add or remove bond sales', 'workscout-child' ),
		'choose_from_most_used'      => __( 'Choose from the most used bond sales', 'workscout-child' ),
		'not_found'                  => __( 'No bond sales found.', 'workscout-child' ),
		'no_terms'                   => __( 'No bond sales', 'workscout-child' ),
		'menu_name'                  => __( 'Bond sales', 'workscout-child' ),
		'items_list_navigation'      => __( 'Bond sales list navigation', 'workscout-child' ),
		'items_list'                 => __( 'Bond sales list', 'workscout-child' ),
		'most_used'                  => _x( 'Most Used', 'bond sale', 'workscout-child' ),
		'back_to_items'              => __( '&larr; Back to bond sales', 'workscout-child' ),
	);
	return $args;
}