<?php
add_action( 'admin_head', 'show_doc_type_colors' );

function show_doc_type_colors() {
	$terms = get_terms( [ 'taxonomy' => 'job_listing_type' ] );
	if ( ! empty( $terms ) ):
		?>
        <style>
            <?php
				foreach ( $terms as $term ) {
					$color = get_term_meta( $term->term_id, 'color', true );
					echo '.job_listing_type span.job-type.'.$term->slug.'{
				background-color:#'.$color.';}';
				}
			?>
        </style>
	<?php
	endif;
}

function filter_cars_by_taxonomies( $post_type, $which ) {

	// Apply this only on a specific post type
	if ( 'job_listing' !== $post_type ) {
		return;
	}

	// A list of taxonomy slugs to filter by
	$taxonomies = [ 'job_listing_type' ];

	foreach ( $taxonomies as $taxonomy_slug ) {

		// Retrieve taxonomy data
		$taxonomy_obj  = get_taxonomy( $taxonomy_slug );
		$taxonomy_name = $taxonomy_obj->labels->name;

		// Retrieve taxonomy terms
		$terms = get_terms( $taxonomy_slug );

		// Display filter HTML
		echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
		echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
		foreach ( $terms as $term ) {
			printf(
				'<option value="%1$s" %2$s>%3$s (%4$s)</option>',
				$term->slug,
				( ( isset( $_GET[ $taxonomy_slug ] ) && ( $_GET[ $taxonomy_slug ] == $term->slug ) ) ? ' selected="selected"' : '' ),
				$term->name,
				$term->count
			);
		}
		echo '</select>';
	}

}

add_action( 'restrict_manage_posts', 'filter_cars_by_taxonomies', 10, 2 );

add_filter('manage_users_columns', 'dm_add_user_id_column');
function dm_add_user_id_column($columns) {
	$columns['dm_user_registered'] = 'User Registered';
	return $columns;
}

add_action('manage_users_custom_column',  'dm_show_user_id_column_content', 10, 3);
function dm_show_user_id_column_content($value, $column_name, $user_id) {
	$user = get_userdata( $user_id );
	if ( 'dm_user_registered' == $column_name )
		return $user->user_registered;
	return $value;
}