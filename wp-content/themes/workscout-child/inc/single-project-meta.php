<?php
// Remove the Jobs meta created by WP Job Manager plugin since it is not needed.
add_action( 'admin_init', function () {
	$wp_job_manager_writepanels = WP_Job_Manager_Writepanels::instance();
	remove_action( 'add_meta_boxes', array( $wp_job_manager_writepanels, 'add_meta_boxes' ) );
} );

// Increase bond value for new post on the basis of last post's bond value.
function workscout_child_increase_bond( $field ) {
	global $post;
	$bond_number = get_option( 'dm_bond_number_count' );
	$bond_number = empty( $bond_number ) ? 1 : $bond_number;

	$assigned_bond_number = get_post_meta( $post->ID, 'bond_number', true );

	if ( empty( $field['value'] ) && empty( $assigned_bond_number ) ) {
		$field['placeholder']   = $bond_number;
		$field['value']         = $bond_number;
		$field['default_value'] = $bond_number;
	}

	return $field;
}

add_filter( 'acf/load_field/name=bond_number', 'workscout_child_increase_bond' );

// Hook the 'workscout_child_increase_bond' when a new issue is created.
//function workscout_child_increase_bond_at_new_post( $new_status, $old_status = null, $post = null ) {
//	if ( $new_status == "auto-draft" ) {
//
//	}
//}

//add_action( 'transition_post_status', 'workscout_child_increase_bond_at_new_post' );

function dm_increase_bond_value() {
	$bond_number = get_field( 'bond_number' );;
	$bond_number_count = get_option( 'dm_bond_number_count' );

	if ( empty( $bond_number ) ) {
		return;
	}

	if ( empty( $bond_number_count ) ) {
		update_option( 'dm_bond_number_count', $bond_number );
	} else if ( $bond_number >= $bond_number_count ) {
		$update_number = $bond_number + 1;
		update_option( 'dm_bond_number_count', $update_number );
	}
}

add_action( 'acf/save_post', 'dm_increase_bond_value', 10 );

function workscout_child_sync_bill_contact_info( $post_id ) {
	$use_the_same_contact = get_field( 'use_the_same_contact' );
	// Sync only when 'use_the_same_contact' is set to true.
	if ( $use_the_same_contact ) {

		// Check if a specific value was sent.
		$fields_arr = array(
			'contact_name'    => 'bill_name',
			'contact_company' => 'bill_company',
			'contact_address' => 'bill_address',
			'contact_city'    => 'bill_city',
			'contact_state'   => 'bill_state',
			'contact_zip'     => 'bill_zip',
			'contact_phone'   => 'bill_phone',
			'contact_email'   => 'bill_email',
		);

		foreach ( $fields_arr as $contact => $bill ) {
			$contact_value = get_field( $contact, $post_id );
			if ( ! empty( $contact_value ) ) {
				update_field( $bill, $contact_value, $post_id );
			}
		}
	}
}

add_action( 'acf/save_post', 'workscout_child_sync_bill_contact_info', 15 );


add_action( 'acf/save_post', 'dm_set_post_password', 10 );


// save_post_job-listing
add_action( 'save_post_job_listing', 'work_scout_child_add_meta_on_save', 10, 2 );
function work_scout_child_add_meta_on_save( $post_id, $post ) {
	//Check it's not an auto save routine
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	//Check it's not an auto save routine
	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	//Perform permission checks! For example:
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}
	// Unhook this function so it doesn't loop infinitely
	remove_action( 'save_post_job_listing', 'work_scout_child_add_meta_on_save', 10 );

	$cusip_rows = get_field( 'cusip' );
	if ( ! empty( $cusip_rows ) ) {
		$cusips = '';
		foreach ( $cusip_rows as $value ) {
			$cusips .= $value['cusip_rep'] . ' ';
		}
		$content = $cusips;
	}

	$issuer_name        = get_the_title( $post_id );
	$content            .= $issuer_name . ' ';
	$post->post_content = $content;
	$result             = wp_update_post( $post );

	// Re-hook this function
	add_action( 'save_post_job_listing', 'work_scout_child_add_meta_on_save', 10, 2 );
}


add_action( 'save_post_job_listing', 'dm_set_post_password' );

function dm_set_post_password( $post_id ) {
	//Check it's not an auto save routine
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	//Check it's not an auto save routine
	if ( wp_is_post_revision( $post_id ) ) {
		return;
	}

	//Perform permission checks! For example:
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return;
	}

	$password = get_field( 'private_access_password' );


	// Unhook this function so it doesn't loop infinitely
	remove_action( 'save_post_job_listing', 'dm_set_post_password' );

	$post_password = ! empty( $password ) ? $password : '';

	$post_array = [ 'ID' => $post_id, 'post_password' => $post_password ];

	$result = wp_update_post( $post_array );

	// Re-hook this function
	add_action( 'save_post_job_listing', 'dm_set_post_password' );

}

// add_filter( 'job_manager_job_listing_wp_admin_fields', 'workscout_child_show_only_expiry_date', 100000 );
// function workscout_child_show_only_expiry_date( $fields ) {
// 	$fields = array(
// 		'_job_expires'     => array(
// 			'label'              => __( 'Listing Expiry Date', 'wp-job-manager' ),
// 			'priority'           => 11,
// 			'show_in_admin'      => true,
// 			'show_in_rest'       => true,
// 			'data_type'          => 'string',
// 			'classes'            => array( 'job-manager-datepicker' ),
// 			'auth_edit_callback' => array( 'WP_Job_Manager_Post_Types', 'auth_check_can_manage_job_listings' ),
// 			'auth_view_callback' => array( 'WP_Job_Manager_Post_Types', 'auth_check_can_edit_job_listings' ),
// 			'sanitize_callback'  => array( 'WP_Job_Manager_Post_Types', 'sanitize_meta_field_date' ),
// 		),
// 	);
// 	return $fields;
// }

// add_action( 'user_register', 'workscout_child_set_user_metaboxes_screen' );
add_action( 'admin_init', 'workscout_child_set_user_metaboxes_screen' );
function workscout_child_set_user_metaboxes_screen( $user_id ) {
    // So this can be used without hooking into user_register
    if ( ! $user_id){
        $user_id = get_current_user_id(); 
    }

    $meta_value_job = array( 'pp_job_settings', 'pp_job_locations', 'postcustom', 'postexcerpt' );
    update_user_option( $user_id, 'metaboxhidden_job_listing', $meta_value_job );
}

function workscout_change_job_title_placeholder( $text, $post ) {
	if ( 'job_listing' === $post->post_type ) {
		return esc_html__( 'Name of Issuer', 'workscout-child' );
	}
	return $text;
}
add_filter( 'enter_title_here','workscout_change_job_title_placeholder', 1, 2 );
