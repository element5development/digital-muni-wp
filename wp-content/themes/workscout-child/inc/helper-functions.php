<?php
function dm_get_full_name() {
	if ( ! is_user_logged_in() ) {
		return false;
	}

	$user = wp_get_current_user();
	$name = $user->first_name . ' ' . $user->last_name;

	return $name;
}

/* This snippet removes the “Listing Expires” column from All Jobs in wp-admin  */
add_filter( 'manage_edit-job_listing_columns', 'workscout_child_remove_expires_column_admin' );
function workscout_child_remove_expires_column_admin( $columns ) {
    unset( $columns['job_expires'] );
    unset( $columns['job_location'] );
    return $columns;
}

function workscout_child_selectively_enqueue_admin_script( $hook ) {

    if ( 'edit.php' != $hook ) {
        return;
    }
    wp_enqueue_style( 'workscout-child-admin', get_stylesheet_directory_uri() . '/css/workscout-child-admin.css', array(), time() );
}
add_action( 'admin_enqueue_scripts', 'workscout_child_selectively_enqueue_admin_script' );

function workscout_child_format_bond_value($value, $post_id, $field) {
	$value = number_format( $value );
	return $value;
}
add_filter('acf/format_value/name=bond_amount', 'workscout_child_format_bond_value', 20, 3);



//  function add_preview_post_row_actions($actions) {
//     global $post;
 
//   $post_type_object = get_post_type_object( $post->post_type );
//   $can_edit_post = current_user_can( $post_type_object->cap->edit_post, $post->ID );
 
//   if (!$can_edit_post && $post->post_status=='draft') {
//     $title = _draft_or_post_title();
//     $actions['view'] = '<a href="' . esc_url( add_query_arg( 'preview', 'true', get_permalink( $post->ID ) ) ) . '" title="' . esc_attr( sprintf( __( 'Preview &#8220;%s&#8221;' ), $title ) ) . '" rel="permalink">' . __( 'Preview' ) . '</a>';
//   }
 
//   return $actions;
 
//   }
 
// if (current_user_can('preview_others_posts')) {
//   add_filter('post_row_actions', 'add_preview_post_row_actions');
// }


// Functions to add password confirmation field in woo registration form.
add_filter( 'woocommerce_registration_errors', 'work_scout_registration_errors_validation', 10,3 );
function work_scout_registration_errors_validation( $reg_errors, $sanitized_user_login, $user_email ) {
	global $woocommerce;
	extract( $_POST );
	if ( strcmp( $password, $password2 ) !== 0 ) {
		return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'workschout-child' ) );
	}
	return $reg_errors;
}

add_action( 'woocommerce_register_form', 'workscout_child_register_form_password_repeat' );
function workscout_child_register_form_password_repeat() {
	?>
	<p class="form-row form-row-wide">
		<label for="reg_password2">
			<?php _e( 'Confirm Password', 'workschout-child' ); ?> 
			<span class="required">*</span>
			<i class="ln ln-icon-Lock-2"></i>
			<input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
		</label>
	</p>
	<?php
}

add_filter('acf/validate_value/name=cusip_rep', 'workscout_child_acf_validate_value', 10, 4);
function workscout_child_acf_validate_value( $valid, $value, $field, $input ){
	// bail early if value is already invalid
	if( !$valid ) {
		return $valid;
	}

	$value_arr = preg_split( '/[\s,]+/', $value );
	foreach ( $value_arr as  $single_str ) {
		if( strlen( $single_str ) != 9 ) {
			$valid = 'One of the strings is not valid';
		}
	}
	// return
	return $valid;
}

// function workscout_child_acf_validate_value( $valid, $value, $field, $input ){
// 	// bail early if value is already invalid
// 	if( !$valid ) {
// 		return $valid;
// 	}

// 	$value_arr = preg_split( '/(\b[a-zA-Z0-9]{9}\b|,|\s)/', $value );

// 	if( ! empty( array_filter( $value_arr ) ) ) {
// 		$valid = 'One of the strings is not valid';
// 	}
// 	// return
// 	return $valid;
// }

function getUserIpAddr(){
	if(!empty($_SERVER['HTTP_CLIENT_IP'])){
		//ip from share internet
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	}elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		//ip pass from proxy
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	}else{
		$ip = $_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}


/**
 * Remove popup login form shortcodes so to override it.
 * @return [type] [description]
 */
add_action( 'init', 'workscout_child_remove_parent_shortcodes' );
function workscout_child_remove_parent_shortcodes() {
	remove_shortcode( 'workscout_login_form' );
}

function workscout_child_login_form() {
 
    if(!is_user_logged_in()) { ?>
        <form method="post" class="login workscout_form">

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <p class="form-row form-row-wide">
                    <label for="username"><?php _e( 'Email address', 'workscout' ); ?> <span class="required">*</span>
                    <i class="ln ln-icon-Male"></i><input type="text" class="input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                    </label>
                </p>
                <p class="form-row form-row-wide">
                    <label for="password"><?php _e( 'Password', 'workscout' ); ?> <span class="required">*</span>
                    <i class="ln ln-icon-Lock-2"></i><input class="input-text" type="password" name="password" id="password" />
                    </label>
                </p>

                <?php do_action( 'woocommerce_login_form' ); ?>

                <p class="form-row">
                    <?php wp_nonce_field( 'woocommerce-login' ); ?>
                    <input type="submit" class="button" name="login" value="<?php esc_attr_e( 'Login', 'workscout' ); ?>" />
                    <label for="rememberme" class="inline">
                        <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'workscout' ); ?>
                    </label>
                </p>
                
                <p class="lost_password">
                    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'workscout' ); ?></a>
                </p>

                <input type="hidden" name="redirect_to" value="<?php echo ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>" />

                <?php do_action( 'woocommerce_login_form_end' ); ?>

            </form>
    <?php }
       
   
}

add_action( 'init', 'workscout_child_add_removed_shortcode' );
function workscout_child_add_removed_shortcode() {
	add_shortcode( 'workscout_login_form', 'workscout_child_login_form' );
}