<?php
add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_state', 10, 2 );
function workscout_child_filter_by_state( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );

		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['wsc-state'] ) ) {
			$wsc_state = sanitize_text_field( $form_data['wsc-state'] );
			$query_args['meta_query']
			           = array(
				array(
					'key'     => 'state_of_issuer',
					'value'   => $wsc_state,
					'compare' => 'IN',
				)
			);
		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_timeframe', 10, 2 );
function workscout_child_filter_by_timeframe( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		// If this is set, we are filtering by salary
		if ( ! empty( $form_data['wsc-filter-timeframe'] ) ) {
			$wsc_filter_timeframe = sanitize_text_field( $form_data['wsc-filter-timeframe'] );
			if ( 'month' == $wsc_filter_timeframe ) {
				$prevmonth  = date( 'm', strtotime( "last month" ) );
				$date_query = absint( $prevmonth );

				$query_args['date_query']
					= array(
					array(
						'month' => $date_query,
					)
				);
			} elseif ( 'year' == $wsc_filter_timeframe ) {
				$last_year  = date( 'Y', strtotime( "last year" ) );
				$date_query = absint( $last_year );
				$query_args['date_query']
				            = array(
					array(
						'year' => $date_query,
					)
				);
			} elseif ( 'week' == $wsc_filter_timeframe ) {
				$last_week  = date( 'W', strtotime( "last week" ) );
				$date_query = absint( $last_week );
				$query_args['date_query']
				            = array(
					array(
						'week' => $date_query,
					)
				);
			} else {
				$query_args['date_query']
					= array(
					array(
						'day' => $date_query,
					)
				);
			}


		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_underwriter', 10, 2 );
function workscout_child_filter_by_underwriter( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if ( ! empty( $form_data['wsc-filter-underwriter'][0] ) ) {
			$wsc_filter_underwriter    = $form_data['wsc-filter-underwriter'];
			$query_args['tax_query'][] = array(
				'taxonomy' => 'underwriter',
				'field'    => 'term_id',
				'terms'    => array_values( $wsc_filter_underwriter ),
				'operator' => 'IN'
			);
		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_ma', 10, 2 );
function workscout_child_filter_by_ma( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if ( ! empty( $form_data['wsc-filter-ma'][0] ) ) {
			$wsc_filter_ma    = $form_data['wsc-filter-ma'];
			$query_args['tax_query'][] = array(
				'taxonomy' => 'ma',
				'field'    => 'term_id',
				'terms'    => array_values( $wsc_filter_ma ),
				'operator' => 'IN'
			);
		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_doc_type', 10, 2 );
function workscout_child_filter_by_doc_type( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if ( ! empty( $form_data['wsc-filter-doc-type'] ) ) {
			$wsc_filter_doc_type = $form_data['wsc-filter-doc-type'];
			$term_id             = [];
			if ( 'preliminary' === $wsc_filter_doc_type ) {
				$preliminary_terms = array( 'preliminary-offering-circular', 'preliminary-offering-statement', 'preliminary-official-statement', 'preliminary-official-statement-notice-of-sale', 'preliminary-remarketing-memorandum', 'preliminary-remarketing-statement' );
				for ( $i = 0; $i <= count( $preliminary_terms ); $i ++ ) {
					$preliminary_term_obj = get_term_by( 'slug', $preliminary_terms[ $i ], 'job_listing_type' );
					$term_id[]            = $preliminary_term_obj->term_id;

				}

			} elseif ( 'final' === $wsc_filter_doc_type ) {
				$final_term_obj = get_term_by( 'slug', 'final-official-statement', 'job_listing_type' );
				$term_id[]      = $final_term_obj->term_id;
			} elseif ( 'other' === $wsc_filter_doc_type ) {
				$other_terms = array( 'supplement', 'addendum', 'notice-of-sale', 'audit', 'private-placement-memo' );
				for ( $i = 0; $i <= count( $other_terms ); $i ++ ) {
					$other_terms_obj = get_term_by( 'slug', $other_terms[ $i ], 'job_listing_type' );
					$term_id[]            = $other_terms_obj->term_id;

				}
			} elseif ( 'investor-pres' === $wsc_filter_doc_type ) {
				$query_args['meta_query'] = array(
			        array(
			            'key'     => 'presentation_embed_code',
			            'value'	  => '',
			            'compare' => '!=',
			        )
			    );
			} else {
				$term_id = [];
			}

			if ( 'investor-pres' != $wsc_filter_doc_type ) {
				if ( count( $term_id ) > 0 ) {
					$query_args['tax_query'][] = array(
						'taxonomy' => 'job_listing_type',
						'field'    => 'term_id',
						'terms'    => $term_id,
						'operator' => 'IN'
					);
				}
			}
		}
	}
	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_keyword', 10, 2 );
function workscout_child_filter_by_keyword( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if ( ! empty( $form_data['wsc-filter-keywords'] ) ) {
			$keyword_type    = $form_data['wsc-filter-keywords'];
			$search_keywords = $form_data['search_keywords'];
			if ( 'exact' == $keyword_type ) {
				$query_args['exact'] = true;
			} elseif ( 'all' == $keyword_type ) {
				$query_args['sentence'] = true;
			} elseif ( 'any' == $keyword_type ) {
				$query_args['exact']    = false;
				$query_args['sentence'] = false;
			}
		}
	}

	return $query_args;
}

add_filter( 'job_manager_get_listings', 'workscout_child_filter_by_per_page', 10, 2 );
function workscout_child_filter_by_per_page( $query_args, $args ) {
	if ( isset( $_POST['form_data'] ) ) {
		parse_str( $_POST['form_data'], $form_data );
		if ( ! empty( $form_data['wsc-filter-perpage'] ) ) {
			$wsc_filter_perpage = absint( $form_data['wsc-filter-perpage'] );
			var_dump( $wsc_filter_perpage);
			die;
			$query_args['posts_per_page'] = $wsc_filter_perpage;
		}
	}

	return $query_args;
}