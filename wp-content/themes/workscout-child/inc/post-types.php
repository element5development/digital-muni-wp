<?php
add_action( 'init', 'dm_register_downloads' );
/**
 * Register a dm-download post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function dm_register_downloads() {
	$labels = array(
		'name'               => _x( 'User Downloads', 'post type general name', 'workscout-child' ),
		'singular_name'      => _x( 'User Download', 'post type singular name', 'workscout-child' ),
		'menu_name'          => _x( 'User Downloads', 'admin menu', 'workscout-child' ),
		'name_admin_bar'     => _x( 'User Download', 'add new on admin bar', 'workscout-child' ),
		'add_new'            => _x( 'Add New', 'dm-download', 'workscout-child' ),
		'add_new_item'       => __( 'Add New User Download', 'workscout-child' ),
		'new_item'           => __( 'New User Download', 'workscout-child' ),
		'edit_item'          => __( 'Edit User Download', 'workscout-child' ),
		'view_item'          => __( 'View User Download', 'workscout-child' ),
		'all_items'          => __( 'All User Downloads', 'workscout-child' ),
		'search_items'       => __( 'Search User Downloads', 'workscout-child' ),
		'parent_item_colon'  => __( 'Parent User Downloads:', 'workscout-child' ),
		'not_found'          => __( 'No downloads found.', 'workscout-child' ),
		'not_found_in_trash' => __( 'No downloads found in Trash.', 'workscout-child' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => __( 'Description.', 'workscout-child' ),
		'public'             => false,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => false,
		'rewrite'            => array( 'slug' => 'dm-download' ),
		'capability_type'    => 'user_download',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'editor' )
	);

	register_post_type( 'dm-download', $args );
}

//add_action( 'wp_ajax_dm_register_file_download', 'dm_register_file_download' );
//add_action( 'wp_ajax_nopriv_dm_register_file_download', 'dm_register_file_download' );

add_action( 'dm_file_downloaded', 'dm_log_file_download', 10 );
function dm_log_file_download( $job_id ) {
	$bond_number = get_field( 'bond_number', $job_id );
	$post_array  = [
		'post_type'   => 'dm-download',
		'post_title'  => wp_strip_all_tags( $bond_number ),
		'post_status' => 'publish',
	];

	$post_id = wp_insert_post( $post_array );

	if ( ! is_wp_error( $post_id ) ) {
		$user        = wp_get_current_user();
		$name        = $user->first_name . ' ' . $user->last_name;
		$email       = $user->user_emaill;
		$issuer_name = get_the_title( $job_id );
		$bond_amount = get_field( 'bond_amount', $job_id );

		update_post_meta( $post_id, 'name_of_issuer', $issuer_name );
		update_post_meta( $post_id, 'bond_amount', $bond_amount );
		update_post_meta( $post_id, 'user_id', get_current_user_id() );
		update_post_meta( $post_id, 'user_name', $name );
		update_post_meta( $post_id, 'user_email', $email );
		update_post_meta( $post_id, 'ip_address', getUserIpAddr() );
	}


}

add_action( 'init', 'dm_download_file', 0 );
function dm_download_file() {
	$attach_id = filter_input( INPUT_GET, 'attach_id' );
	$job_id    = filter_input( INPUT_GET, 'job_id' );
	if ( empty( $attach_id ) ) {
		return false;
	}

	$file = get_attached_file( $attach_id );
	ob_end_flush();
	if ( ! file_exists( $file ) ) {
		return false;
	} else {
		do_action( 'dm_file_downloaded', $attach_id, $job_id );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Type: application/octet-stream' );
		header( 'Content-Disposition: attachment; filename="' . basename( $file ) . '"' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate' );
		header( 'Pragma: public' );
		header( 'Content-Length: ' . filesize( $file ) );
		readfile( $file );

		exit;
	}
}

add_filter( 'acf/settings/remove_wp_meta_box', '__return_false' );

add_filter( 'manage_dm-download_posts_columns', 'dm_set_download_custom_columns' );
add_action( 'manage_dm-download_posts_custom_column', 'dm_custom_download_column', 10, 2 );

function dm_set_download_custom_columns( $columns ) {
	unset( $columns['date'] );
	$columns['name_of_issuer'] = __( 'Name of Issuer', 'your_text_domain' );
	$columns['bond_amount']    = __( 'Issue Amount', 'your_text_domain' );
	//$columns['user_name']      = 'User Name';
	$columns['user_email']     = 'Email';

	$columns['downloaded_date'] = 'Downloaded On';
	$columns['ip_address'] = 'User IP';

	return $columns;
}

function dm_custom_download_column( $column, $post_id ) {
	switch ( $column ) {
		case 'name_of_issuer' :
			echo get_post_meta( $post_id, 'name_of_issuer', true );
			break;
		case 'bond_amount' :
			echo get_post_meta( $post_id, 'bond_amount', true );
			break;
		case 'user_name' :
			$user_id = get_post_meta( $post_id, 'user_id', true );
			$user    = get_user_by( 'id', $user_id );
			echo $user->first_name . ' ' . $user->last_name;
			break;
		case 'user_email':
			$user_id = get_post_meta( $post_id, 'user_id', true );
			$user    = get_user_by( 'id', $user_id );
			echo $user->user_email;
			break;
		case 'downloaded_date':
			echo get_the_date( '', $post_id );
			break;
		case 'ip_address':
			echo get_post_meta( $post_id, 'ip_address', true );
			break;
	}
}

add_filter( 'register_post_type_job_listing', 'workscout_child_rename_job_listing_post_type' );
function workscout_child_rename_job_listing_post_type( $args ) {
	$singular         = __( 'Project', 'wp-job-manager' );
	$plural           = __( 'Projects', 'wp-job-manager' );
	$args['labels']   = array(
		'name'               => $plural,
		'singular_name'      => $singular,
		'menu_name'          => __( 'Projects', 'wp-job-manager' ),
		// translators: Placeholder %s is the plural label of the job listing post type.
		'all_items'          => sprintf( __( 'All %s', 'wp-job-manager' ), $plural ),
		'add_new'            => __( 'Add New', 'wp-job-manager' ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'add_new_item'       => sprintf( __( 'Add %s', 'wp-job-manager' ), $singular ),
		'edit'               => __( 'Edit', 'wp-job-manager' ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'edit_item'          => sprintf( __( 'Edit %s', 'wp-job-manager' ), $singular ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'new_item'           => sprintf( __( 'New %s', 'wp-job-manager' ), $singular ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'view'               => sprintf( __( 'View %s', 'wp-job-manager' ), $singular ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'view_item'          => sprintf( __( 'View %s', 'wp-job-manager' ), $singular ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'search_items'       => sprintf( __( 'Search %s', 'wp-job-manager' ), $plural ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'not_found'          => sprintf( __( 'No %s found', 'wp-job-manager' ), $plural ),
		// translators: Placeholder %s is the plural label of the job listing post type.
		'not_found_in_trash' => sprintf( __( 'No %s found in trash', 'wp-job-manager' ), $plural ),
		// translators: Placeholder %s is the singular label of the job listing post type.
		'parent'             => sprintf( __( 'Parent %s', 'wp-job-manager' ), $singular ),
	);
	$args['supports'] = array( 'title', 'editor', 'custom-fields', 'publicize' );

	return $args;
}