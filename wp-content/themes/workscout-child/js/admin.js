(function ($) {

    acf.add_action('ready', function ($el) {

        //Identify the field you want to check against.
        //Use a class to handle scenarios involving repeater / flexible content where multiple instances exist
        var $field = $('#type_of_document').find('input');

        $field.on('change', function (evt) {

            //Comparing against taxonomy field values to determine which other fields to show / hide
            switch ($(this).val()) {
                case '55':
                    //A semi-strange "up over down" jQuery to ensure if inside a repeater / flexibl content you show/hide only the desired field
                    alert('x');
                    $(this).closest(".example").siblings(".conditional-display").removeClass("hidden-by-conditional-logic");
                    break;
                default:
                    //All other states, lets ensure they are hidden
                    $(this).closest(".example").siblings(".conditional-display").addClass("hidden-by-conditional-logic");
            }

        });

        //On initial page load - ACF data may indicate that a field should be shown instead of hidden
        //Each ensures it craws for all repeaters / flexible content
        $field.each(function (index) {
            if ($(this).val() == '55') {
                $(this).closest(".example").siblings(".conditional-display").removeClass("hidden-by-conditional-logic");
            }
        });

    });

})(jQuery);