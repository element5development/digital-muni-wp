/* ----------------- Start Document ----------------- */
(function ($) {
    jQuery.validator.addMethod(
        "multiemails",
        function (value, element) {
            if (this.optional(element)) { // return true on optional element
                return true;
            }
            ;
            var emails = value.split(/[;,]+/); // split element by , and ;
            valid = true;
            for (var i in emails) {
                value = emails[i];
                valid = valid &&
                    jQuery.validator.methods.email.call(this, $.trim(value), element);
            }
            return valid;
        },

        jQuery.validator.messages.multiemails
    );

    var searchFilter = {
        init: function () {
            //cache dom
            this.$sidebar = $('#dm-search-sidebar');
            this.$mobileSearchbar = $('#mobile-search');
            this.$searchFilter = $('#dm-search-filters-container');
            this.windowWidth = $(window).width();
            this.$toggleSearch = $('#dm-show-advanced-search');

            $('.dm-popup').magnificPopup({
                type: 'inline'
                // other options
            });

            //this.$toggleSearch.on('click', this.toggleSearch.bind(this));
            //$(window).on('resize', this.moveToProperLocation.bind(this));

            //  this.moveToProperLocation();


        },
        moveToProperLocation: function (e) {
            this.windowWidth = $(window).width();
            if (this.windowWidth < 768) {
                this.$searchFilter.appendTo(this.$mobileSearchbar).hide();
            } else {
                this.$searchFilter.appendTo(this.$sidebar).show();
            }
        },
        toggleSearch: function (e) {
            e.preventDefault();
            if (this.windowWidth > 767) {
                return false;
            }
            this.$searchFilter.slideToggle();
        }

    };

    //show popup
    var downloadPopUp = {
        init: function () {
            this.cacheDOM();
            this.eventListeners();
        },
        cacheDOM: function () {
            this.$html = $('html');
            this.$body = $('body');
        },
        eventListeners: function () {
            //on listing pages
            this.$body.on('click', '.job_listings li a.job_listing, .job_listings li .buttons .open-download-notice-popup', this.downloadNoticePopUp);
            this.$body.on('click', '#download-notice .download-notice-checkbox', this.enableListAcceptBtn);
            this.$body.on('click', '.open-mini-info-popup', this.openInfoPopup);
            this.$body.on('click', '.open-presentation-popup', this.openPresentationPopup);

            //when visitor opens bond page directly
            $('body.page-template-tmpl-download-notice .download-notice-checkbox').on('click', this.enableSingleAcceptBtn);

        },
        openPresentationPopup: function (e) {
            e.preventDefault();
            var project_id = $(this).data('project_id');
            $.ajax({
                url: dm_data.ajax_url,
                type: 'POST',
                data: {action: 'dm_get_embedded_pdf', project_id: project_id},
                beforesend: function () {

                },
                success: function (response) {
                    if (response.success === true) {
                        var desktop_presentation = response.data.pdf_desktop;
                        var mobile_presentation = response.data.pdf_mobile;
                        // Open directly via API
                        $.magnificPopup.open({
                            items: {
                                src: ' <div class="dm-presentation small-dialog zoom-anim-dialog apply-popup"><div class="small-dialog-content">' + desktop_presentation + '</div></div>', // can be a HTML string, jQuery object, or CSS selector
                                type: 'inline'
                            }
                        });
                    } else {
                        console.log(response);
                    }
                }
            });
        },
        downloadNoticePopUp: function (e) {
            e.preventDefault();
            $.magnificPopup.open({
                type: 'inline',
                items: {
                    src: $(this).closest('li').find('#download-notice')
                }
            });
        },
        enableListAcceptBtn: function () {
            var $downloadNotice = $(this).closest('#download-notice');
            var $downloadNoticeButton = $downloadNotice.find('.download-notice-btn');

            if ($(this).is(':checked')) {
                var url = $downloadNotice.find('.download-notice-btn').data('href');
                $downloadNoticeButton.removeClass('disabled').attr('href', url);
            } else {
                $downloadNoticeButton.addClass('disabled').attr('href', '#');
            }
        },
        enableSingleAcceptBtn: function () {

            if ($(this).is(':checked')) {
                var url = $(this).parent().next('.download-notice-btn').data('href');
                $(this).parent().next('.download-notice-btn').removeClass('disabled').attr('href', url);
            } else {
                $(this).parent().next('.download-notice-btn').addClass('disabled').attr('href', '#');
            }
        },
        openInfoPopup: function (e) {
            e.preventDefault();
            $.magnificPopup.open({
                type: 'inline',
                items: {
                    src: $(this).closest('li').find('#mini-info')
                }
            });
        }
    };


    var shareBond = {
        init: function () {
            this.cacheDOM();
            this.eventListeners();
            this.hideEmail();
            this.validateForm();
            this.popup();
        },
        cacheDOM: function () {
            this.$html = $('html');
            this.$body = $('body');
            this.$shareBondForm = this.$body.find('form#share-bond');
            this.$shareKind = this.$shareBondForm.find('#share_kind');
            this.$shareEmail = this.$shareBondForm.find('.share-mail-wrap');
        },
        hideEmail: function () {
            var shareKindValue = this.$shareKind.val();
            if ('self' === shareKindValue) {
                this.$shareEmail.hide();
            }
        },
        eventListeners: function () {
            $(this.$shareKind).on('change', this.toggleEmail.bind(this));
        },
        toggleEmail: function () {
            var shareKindValue = this.$shareKind.val();
            if ('self' === shareKindValue) {
                this.$shareEmail.hide(200);
            } else {
                this.$shareEmail.show(200);
            }
        },
        validateForm: function () {
            this.$shareBondForm.validate({
                rules: {
                    share_email: {
                        required: true,
                        multiemails: true
                    }
                },
                messages: {
                    share_email: {
                        required: "This is required",
                        multiemails: "Invalid email."
                    }
                }
            });
        },
        popup: function () {
            if ($("#mail-success").length) {
                $.magnificPopup.open({
                    items: {
                        src: '#mail-success'
                    },
                    type: 'inline',
                });
            }
        }
    };

    DIGITALMUNI = {
        common: {
            init: function () {
                console.log('start of page document ready');
            },
            finalize: function () {
                console.log('at the end of document ready');
            }
        },
        'home': {
            init: function () {
                downloadPopUp.init();
                searchFilter.init();
            }
        },
        'single-job_listing': {
            init: function () {
                shareBond.init();
                downloadPopUp.init();
            }
        },
        'page-template-tmpl-download-notice': {
            init: function () {
                downloadPopUp.init();
            }
        }
    };

    //common UTIL this doesn't change
    CM_JS_UTIL = {
        fire: function (func, funcname, args) {
            var namespace = DIGITALMUNI; // indicate your obj literal namespace here for standard lets make it abbreviation of current project
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }

        },

        loadEvents: function () {

            var bodyId = document.body.id;

            // hit up common first.
            CM_JS_UTIL.fire('common');

            // do all the classes too.
            $.each(document.body.className.split(/\s+/), function (i, classnm) {
                CM_JS_UTIL.fire(classnm);
                CM_JS_UTIL.fire(classnm, bodyId);
            });

            CM_JS_UTIL.fire('common', 'finalize');

        }
    };


    $(function () {
        CM_JS_UTIL.loadEvents();

    });

})(jQuery);
