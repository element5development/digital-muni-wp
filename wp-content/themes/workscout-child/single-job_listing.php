<?php
/**
 * The template for displaying all single jobs.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WorkScout
 */

get_header(); ?>
<?php while ( have_posts() ) :
	the_post();
	?>
    <!-- Titlebar -->
	<?php
	$header_image = get_post_meta( $post->ID, 'pp_job_header_bg', true );

	if ( ! empty( $header_image ) ) {
		$transparent_status = get_post_meta( $post->ID, 'pp_transparent_header', true );
		if ( $transparent_status == 'on' ) { ?>
            <div id="titlebar" class="photo-bg with-transparent-header" style="background: url('<?php echo esc_url( $header_image ); ?>')">
		<?php } else { ?>
            <div id="titlebar" class="photo-bg" style="background: url('<?php echo esc_url( $header_image ); ?>')">
		<?php } ?>
	<?php } else { ?>
        <div id="titlebar" class="single">
	<?php } ?>

    <div class="container">
        <div class="eleven columns">

			<?php
			$terms = get_the_terms( $post->ID, 'job_listing_category' );

			if ( $terms && ! is_wp_error( $terms ) ) :

				$jobcats = array();

				foreach ( $terms as $term ) {
					$term_link = get_term_link( $term );
					$jobcats[] = '<a href="' . $term_link . '">' . $term->name . '</a>';
				}

				$print_cats = join( " / ", $jobcats ); ?>
				<?php echo '<span>' . $print_cats . '</span>'; ?>
			<?php
			endif;
			$qualified_text_color = get_field('qualified_under_color','option');
			?>
            <h1>Download Info</h1>
            <span class="dm-qualified"><a target="_blank" href="https://emma.msrb.org" style="color:<?php echo $qualified_text_color; ?>;">Qualified Portal Under MSRB Rule G-32</a></span>
        </div>

        <div class="five columns">
			<?php do_action( 'workscout_bookmark_hook' ) ?>

        </div>

    </div>
    </div>


    <!-- Content
	================================================== -->
	<?php
	if ( ! empty( post_password_required() ) ) {
		?>
        <div class="container">
            <div class="sixteen columns">
				<?php echo get_the_password_form(); ?>
            </div>
        </div>
		<?php
	} else{
		?>
		<?php

		$layout = Kirki::get_option( 'workscout', 'pp_job_layout' ); ?>
        <div class="container <?php echo esc_attr( $layout ); ?>">
            <div class="sixteen columns">
				<?php do_action( 'job_content_start' ); ?>
            </div>

			<?php if ( class_exists( 'WP_Job_Manager_Applications' ) ) : ?>
				<?php if ( is_position_filled() ) : ?>
                    <div class="sixteen columns">
                        <div class="notification closeable notice "><?php esc_html_e( 'This position has been filled', 'workscout' ); ?></div>
                        <div class="margin-bottom-35"></div>
                    </div>
				<?php elseif ( ! candidates_can_apply() && 'preview' !== $post->post_status ) : ?>
                    <div class="sixteen columns">
                        <div class="notification closeable notice "><?php esc_html_e( 'Applications have closed', 'workscout' ); ?></div>
                    </div>
				<?php endif; ?>
			<?php endif; ?>

            <!-- Recent Jobs -->
			<?php $logo_position = Kirki::get_option( 'workscout', 'pp_job_list_logo_position', 'left' ); ?>
			<?php
			if ( isset( $_GET['share'] ) ) {  // isset share open
				get_template_part( 'share-form/content', 'form' );
			} else { ?>
                <div class="eleven columns " id="dm-job-details">
                    <div class="padding-right">
						<?php if ( get_the_company_name() ) { ?>
                            <!-- Company Info -->
                            <div class="company-info <?php echo ( $logo_position == 'left' ) ? 'left-company-logo' : 'right-company-logo'; ?>">
                                <div class="content">
                                    <h4>
										<?php if ( class_exists( 'Astoundify_Job_Manager_Companies' ) ) {
											echo workscout_get_company_link( the_company_name( '', '', false ) );
										} ?>
										<?php the_company_name( '<strong>', '</strong>' ); ?>
										<?php if ( class_exists( 'Astoundify_Job_Manager_Companies' ) ) {
											echo "</a>";
										} ?>
										<?php the_company_tagline( '<span class="company-tagline">- ', '</span>' ); ?></h4>
									<?php if ( $website = get_the_company_website() ) : ?>
                                        <span><a class="website" href="<?php echo esc_url( $website ); ?>" target="_blank" rel="nofollow"><i class="fa fa-link"></i> <?php esc_html_e( 'Website', 'workscout' ); ?></a></span>
									<?php endif; ?>
									<?php if ( get_the_company_twitter() ) : ?>
                                        <span><a href="http://twitter.com/<?php echo get_the_company_twitter(); ?>">
								<i class="fa fa-twitter"></i>
								@<?php echo get_the_company_twitter(); ?>
							</a></span>
									<?php endif; ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
						<?php } ?>


                        <div class="single_job_listing">

							<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
                                <div class="job-manager-info"><?php esc_html_e( 'This listing has expired.', 'workscout' ); ?></div>
							<?php else : ?>
                                <h3><?php the_title(); ?></h3>
                                <div class="project-meta">
                                    <span><i class="ln dm-icon-marker"></i> <?php the_field( 'state_of_issuer' ); ?></span>
                                    <span><i class="ln dm-icon-calendar"></i> <?php the_date( get_option( 'date_format' ) ); ?> <?php the_time( get_option( 'time_format' ) ); ?></span>
                                </div>
                                <hr>
								<?php
								$document_type_obj = get_field( 'type_of_document' );
								if ( ! empty( $document_type_obj ) ) {
									?>
                                    <p><a style="color:#<?php echo get_term_meta( $document_type_obj->term_id, 'color', true );?>" href="<?php echo esc_url( get_term_link( $document_type_obj, 'job-listing' ) ); ?>"><?php echo $document_type_obj->name; ?></a></p>
								<?php } ?>

                                <div class="job_description">
                                	<?php the_field( 'bond_content' ); ?>
									<?php do_action( 'workscout_single_job_before_content' ); ?>
									<?php //the_company_video(); ?>
									<?php //echo do_shortcode( apply_filters( 'the_job_description', get_the_content() ) ); ?>
                                </div>
                                <p>
                                    <strong>Sale Type </strong>
									<?php
									$sale_type = get_field( 'type_of_bond_sale' );
									echo $sale_type->name;
									?>
                                </p>

                                <p>
                                    <strong>Underwriter(s) </strong>
									<?php
									$underwriters_obj = get_field( 'underwriters' );
									foreach ( $underwriters_obj as $underwriters ) { 
										$underwriters_name[] = $underwriters->name;
									} 
									echo implode( ", ", $underwriters_name );
									?>
                                </p>

                                <?php 
                                foreach ( $underwriters_obj as $underwriters ) { 
	                            	$underwriters_featured_image = get_field( 'underwriters_featured_image', $underwriters );
	                            	if ( ! empty( $underwriters_featured_image ) ) {
	                            	$width = get_field('uw_logo_width',$underwriters);
	                            	$width = !empty($width) ? $width : '100';
	                            	    ?>
	                                <p>
	                                	<img style="width: <?php echo $width.'px'; ?>" src="<?php echo esc_url( $underwriters_featured_image ); ?>">
	                                </p>
									<?php } 
								} ?>

                                <p>
                                    <strong>Municipal Advisor(s)</strong>
									<?php
									$mas_obj = get_field( 'mas' );
									foreach ( $mas_obj as $mas ) { 
										$mas_name[] = $mas->name;
									} 
									echo implode( ", ", $mas_name );
									?>
                                </p>

                            	<?php 
                            	foreach ( $mas_obj as $mas ) { 
	                            	$ma_featured_image = get_field( 'ma_featured_image', $mas );
	                            	if ( ! empty( $ma_featured_image ) ) {
	                            	$width = get_field('ma_logo_width',$mas);
	                            	$width = !empty($width) ? $width : '100';
	                            	    ?>
	                                <p>
	                                	<img style="width: <?php echo $width.'px'; ?>" src="<?php echo esc_url( $ma_featured_image ); ?>">
	                                </p>
									<?php } 
								} ?>

                                <p><strong>Published at </strong><?php echo get_the_date( get_option( 'date_format' ) ); ?> <?php the_time( get_option( 'time_format' ) ); ?></p>

                                <p>
                                    <strong>Document size </strong>
									<?php
									$attachment_obj = get_field( 'upload_document' );
									echo size_format( $attachment_obj['filesize'] );
									?>
                                </p>

                                <p>
                                <strong>Ratings </strong>
                                <?php

                                //the_field('ratings');
                                 $ratings = get_field('ratings');
                                 $ratings = explode(',',$ratings);
                                 foreach($ratings as $rating){
                                     echo trim($rating).'<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                 }
//                                 $ratings = explode(',',$ratings);
//                                 $matches = [];
//                                 if(!empty($ratings))
//                                     {
//                                         foreach($ratings as $rating){
//                                             $rating = trim($rating);
//                                         preg_match('/[^:]*/',$rating,$matches);
//                                         $rating = str_replace($matches,'<strong>'.$matches[0].'</strong>',$rating);
//                                         echo str_replace(':', ' ',$rating).'<br />';
//                                     }
//                                 }
                                 ?>
                                 </p>
								<?php
								/**
								 * single_job_listing_end hook
								 */
								do_action( 'single_job_listing_end' );
								?>

								<?php
								$share_options = Kirki::get_option( 'workscout', 'pp_job_share' );

								if ( ! empty( $share_options ) ) {
									$id       = $post->ID;
									$title    = urlencode( $post->post_title );
									$url      = urlencode( get_permalink( $id ) );
									$summary  = urlencode( workscout_string_limit_words( $post->post_excerpt, 20 ) );
									$thumb    = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'medium' );
									$imageurl = urlencode( $thumb[0] );
									?>
                                    <ul class="share-post">
										<?php if ( in_array( "facebook", $share_options ) ) { ?>
                                            <li><?php echo '<a target="_blank" class="facebook-share" href="https://www.facebook.com/sharer/sharer.php?u=' . $url . '">Facebook</a>'; ?></li><?php } ?>
										<?php if ( in_array( "twitter", $share_options ) ) { ?>
                                            <li><?php echo '<a target="_blank" class="twitter-share" href="https://twitter.com/share?url=' . $url . '&amp;text=' . esc_attr( $summary ) . '" title="' . __( 'Twitter', 'workscout' ) . '">Twitter</a>'; ?></li><?php } ?>
										<?php if ( in_array( "google-plus", $share_options ) ) { ?>
                                            <li><?php echo '<a target="_blank" class="google-plus-share" href="https://plus.google.com/share?url=' . $url . '&amp;title="' . esc_attr( $title ) . '" onclick=\'javascript:window.open(this.href, "", "menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600");return false;\'>Google Plus</a>'; ?></li><?php } ?>
										<?php if ( in_array( "pinterest", $share_options ) ) { ?>
                                            <li><?php echo '<a target="_blank"  class="pinterest-share" href="http://pinterest.com/pin/create/button/?url=' . $url . '&amp;description=' . esc_attr( $summary ) . '&media=' . esc_attr( $imageurl ) . '" onclick="window.open(this.href); return false;">Pinterest</a>'; ?></li><?php } ?>
										<?php if ( in_array( "linkedin", $share_options ) ) { ?>
                                            <li><?php echo '<a target="_blank"  class="linkedin-share" href="https://www.linkedin.com/cws/share?url=' . $url . '">LinkedIn</a>'; ?></li><?php } ?>

                                        <!-- <li><a href="#add-review" class="rate-recipe">Add Review</a></li> -->
                                    </ul>
								<?php } ?>
                                <div class="clearfix"></div>
                                <div class="buttons">
                                <?php if(!empty($attachment_obj)){
                                    $permalink = get_permalink();
                                    $link      = add_query_arg( [
                                        'accepted'   => 'true',
                                        'show_pdf'   => 'true',
                                        'project_id' => get_the_ID()
                                    ], $permalink );
                                    ?>
                                    <a href="<?php echo $link; ?>" class="download-open-pdf button" target="_blank"><?php $button_text = get_field( 'download_pdf_button_text_single_page', 'option' );
				echo ! empty( $button_text ) ? $button_text : 'Download';
				?></a>
                                    <?php } ?>

                                    <a href="<?php echo get_permalink() . '?accepted=true&share=1' ?>" class="send-email button"><?php esc_html_e( 'Send Email', 'workscout' ) ?></a>
                                                           <?php
                                $presentation_embed_code = get_field('presentation_embed_code' );
                                 if ( ! empty( $presentation_embed_code ) ) {
                                     ?>
                                      <a href="#" class="open-presentation-popup button" title="open presentation" data-project_id="<?php echo get_the_ID(); ?>">
                    <i class="fa fa-video-camera"></i>
                </a>
                                  <?php
								}
								?>
                                </div>



							<?php endif; ?>

							<?php
							$related = Kirki::get_option( 'workscout', 'pp_enable_related_jobs' );

							if ( $related ) {
								get_template_part( 'template-parts/jobs-related' );
							} ?>

                        </div>
                    </div>
                </div>


                <!-- Widgets -->
                <div class="five columns" id="job-details">
					<?php dynamic_sidebar( 'sidebar-job-before' ); ?>
                    <!-- Sort by -->
                    <?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' != $post->post_status ) : ?>
                    <div class="widget">
                        <h4><?php esc_html_e( 'Overview', 'workscout' ) ?></h4>
						<?php $overview_elements = Kirki::get_option( 'workscout', 'pp_job_overview', array( 'date_posted', 'expiration_date', 'application_deadline', 'location', 'job_title', 'hours', 'rate', 'salary' ) ); ?>
                        <div class="job-overview digthis">
							<?php do_action( 'single_job_listing_meta_before' ); ?>

							<?php do_action( 'single_job_listing_meta_start' ); ?>
                            <div class="preview-bond">
								<?php
								$overview_img = wp_get_attachment_image( $attachment_obj['ID'], 'full' );
								//keep hierarchy manual image > pdf image preview > auto generate

								$manual_preview_image    = get_field( 'manual_preview_image' );


								if ( ! empty( $manual_preview_image ) ) {
									echo '<img src="' . $manual_preview_image . '">';
								}  else {
									echo $overview_img;
								} ?>
                                <!-- <i class="fa fa-user"></i> -->
                                <div>
                                    <strong><?php //esc_html_e('Job Title','workscout');
										?></strong>
                                    <span><?php //the_title();
										?></span>
                                </div>
                            </div>
							<?php do_action( 'single_job_listing_meta_end' ); ?>
                        </div>

                    </div>
					<?php endif; ?>
					<?php
					$single_map = Kirki::get_option( 'workscout', 'pp_enable_single_jobs_map' );
					$lng        = $post->geolocation_long;
					if ( $single_map && ! empty( $lng ) ) :
						?>

                        <div class="widget">
                            <h4><?php esc_html_e( 'Job Location', 'workscout' ) ?></h4>

                            <div id="job_map" data-longitude="<?php echo esc_attr( $post->geolocation_long ); ?>" data-latitude="<?php echo esc_attr( $post->geolocation_lat ); ?>">

                            </div>
                        </div>

					<?php
					endif;
					dynamic_sidebar( 'sidebar-job-after' ); ?>

                </div>
                <!-- Widgets / End -->
			<?php } // isset share close
			?>

        </div>
        <div class="clearfix"></div>
        <div class="margin-top-55"></div>

		<?php
	}
	endwhile; // End of the loop.
get_footer();
